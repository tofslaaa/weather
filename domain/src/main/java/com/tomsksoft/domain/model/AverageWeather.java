package com.tomsksoft.domain.model;

public class AverageWeather {

    private final double mTemperature;
    private final double mWindSpeed;
    private final double mWindDirection;
    private final double mPressure;
    private final double mHumidity;
    private final Cloudiness mCloudiness;

    public AverageWeather(
            final double temperature,
            final double windSpeed,
            final double windDirection,
            final double pressure,
            final double humidity,
            final Cloudiness cloudiness) {
        mTemperature = temperature;
        mWindSpeed = windSpeed;
        mWindDirection = windDirection;
        mPressure = pressure;
        mHumidity = humidity;
        mCloudiness = cloudiness;
    }

    public double getTemperature() {
        return mTemperature;
    }

    public double getWindSpeed() {
        return mWindSpeed;
    }

    public double getPressure() {
        return mPressure;
    }

    public double getHumidity() {
        return mHumidity;
    }

    public Cloudiness getCloudiness() {
        return mCloudiness;
    }

    public double getWindDirection() {
        return mWindDirection;
    }

    @Override
    public String toString() {
        return "AverageWeather{" +
               "mTemperature=" + mTemperature +
               ", mWindSpeed=" + mWindSpeed +
               ", mWindDirection=" + mWindDirection +
               ", mPressure=" + mPressure +
               ", mHumidity=" + mHumidity +
               ", mCloudiness=" + mCloudiness +
               '}';
    }
}
