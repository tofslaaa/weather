package com.tomsksoft.domain.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Forecasts {

    private final Map<Long, Forecast> mForecasts;

    public Forecasts(final List<Forecast> forecasts) {
        mForecasts = new HashMap<>();
        for (final Forecast forecast : forecasts) {
            mForecasts.put(forecast.getMillis(), forecast);
        }
    }

    public Map<Long, Forecast> getForecasts() {
        return mForecasts;
    }

    public Forecast getForecastByMillis(final long millis) {
        return mForecasts.get(millis);
    }

    public static class Forecast {

        private final long mMillis;
        private final Weather mWeather;

        public Forecast(final long millis, final Weather weather) {
            mMillis = millis;
            mWeather = weather;
        }

        public long getMillis() {
            return mMillis;
        }

        public Weather getWeather() {
            return mWeather;
        }
    }
}
