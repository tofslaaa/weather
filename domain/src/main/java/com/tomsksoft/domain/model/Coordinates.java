package com.tomsksoft.domain.model;

public class Coordinates {

    private final double lon;
    private final double lat;

    public Coordinates(final double lon, final double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
               "lon=" + lon +
               ", lat=" + lat +
               '}';
    }
}
