package com.tomsksoft.domain.model;

public enum Cloudiness {
    CLEAR,
    CLOUDY,
    OVERCAST,
    SLEET,
    RAIN,
    SNOW,
    NONE
}
