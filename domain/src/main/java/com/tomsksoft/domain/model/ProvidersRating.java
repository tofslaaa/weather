package com.tomsksoft.domain.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ProvidersRating {

    private final Map<Provider, List<Double>> mRatings;

    public ProvidersRating(final Map<Provider, List<Double>> ratings) {
        mRatings = ratings;
    }

    public void addDiff(final Provider provider, final double diff) {
        List<Double> diffs = mRatings.get(provider);
        if (diffs == null) {
            diffs = new ArrayList<>(1);
        }
        diffs.add(diff);
        mRatings.put(provider, diffs);
    }

    public Map<Provider, List<Double>> getRatings() {
        return mRatings;
    }

    public Provider getBestProvider(){
        final TreeMap<Double, Provider> ratingMap = new TreeMap<>();
        for (final Map.Entry<Provider, List<Double>> entry : mRatings.entrySet()) {
            double sum = 0;
            for (final Double diff : entry.getValue()) {
                sum += diff;
            }
            double rating = sum / entry.getValue().size();
            ratingMap.put(rating, entry.getKey());
        }

        final Provider bestProvider = ratingMap.firstEntry().getValue();
        return bestProvider;
    }
}
