package com.tomsksoft.domain.model;

public class Weather {

    private final double mTemperature;
    private final String mLocation;
    private final double mWindSpeed;
    private final double mPressure;
    private final double mHumidity;
    private final Cloudiness mCloudiness;
    private final double mWindDirection;

    public Weather(
            final double temperature,
            final String location,
            final double windSpeed,
            final double windDirection,
            final double pressure,
            final double humidity,
            final Cloudiness cloudiness) {
        mTemperature = temperature;
        mLocation = location;
        mWindSpeed = windSpeed;
        mWindDirection = windDirection;
        mPressure = pressure;
        mHumidity = humidity;
        mCloudiness = cloudiness;
    }

    public double getTemperature() {
        return mTemperature;
    }

    public String getLocation() {return mLocation;}

    public Cloudiness getCloudiness() {return mCloudiness;}

    public double getWindSpeed() {
        return mWindSpeed;
    }

    public double getPressure() {
        return mPressure;
    }

    public double getHumidity() {
        return mHumidity;
    }

    public double getWindDirection() {
        return mWindDirection;
    }

    @Override
    public String toString() {
        return "Weather{" +
               "mTemperature=" + mTemperature +
               ", mLocation='" + mLocation + '\'' +
               ", mWindSpeed=" + mWindSpeed +
               ", mPressure=" + mPressure +
               ", mHumidity=" + mHumidity +
               ", mCloudiness=" + mCloudiness +
               ", mWindDirection=" + mWindDirection +
               '}';
    }
}
