package com.tomsksoft.domain.model;

public class AverageForecast {

    private final long mMillis;
    private final double mTemperature;
    private final Cloudiness mCloudiness;

    public AverageForecast(final long millis,
                           final double temperature,
                           final Cloudiness cloudiness) {
        mMillis = millis;
        mTemperature = temperature;
        mCloudiness = cloudiness;
    }

    public long getMillis() {
        return mMillis;
    }

    public double getTemperature() {
        return mTemperature;
    }

    public Cloudiness getCloudiness() {
        return mCloudiness;
    }

    @Override
    public String toString() {
        return "AverageForecast{" +
               "mMillis=" + mMillis +
               ", mTemperature=" + mTemperature +
               ", mCloudiness=" + mCloudiness +
               '}';
    }
}
