package com.tomsksoft.domain.model;

public class UserReport {

    private final Cloudiness mCloudiness;
    private final double mTemperature;
    private final WindType mWind;

    public UserReport(final Cloudiness cloudiness, final double temperature, final WindType wind) {
        mCloudiness = cloudiness;
        mTemperature = temperature;
        mWind = wind;
    }

    public Cloudiness getCloudiness() {
        return mCloudiness;
    }

    public double getTemperature() {
        return mTemperature;
    }

    public WindType getWind() {
        return mWind;
    }
}
