package com.tomsksoft.domain.model;

public enum WindType {
    CALM,
    LIGHT,
    MODERATE,
    STRONG,
    NONE
}
