package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IOpenWeatherRepository;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.Forecasts;

import java.util.concurrent.ExecutorService;

public class RequestOpenWeatherForecastInteractor extends RequestInteractor<Coordinates, Forecasts> {

    private final IOpenWeatherRepository mOpenWeatherRepository;

    public RequestOpenWeatherForecastInteractor(final ExecutorService executor,
                                                final IOpenWeatherRepository openWeatherRepository) {
        super(executor);
        mOpenWeatherRepository = openWeatherRepository;
    }

    @Override
    protected Forecasts buildUseCase(final Coordinates params) throws Throwable {
        return mOpenWeatherRepository.getForecasts(params.getLon(), params.getLat());
    }
}
