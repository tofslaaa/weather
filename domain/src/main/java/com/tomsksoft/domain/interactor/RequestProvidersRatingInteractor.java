package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IRatingRepository;
import com.tomsksoft.domain.model.ProvidersRating;

import java.util.concurrent.ExecutorService;

public class RequestProvidersRatingInteractor extends RequestInteractor<Boolean, ProvidersRating> {

    private final IRatingRepository mRatingRepository;

    public RequestProvidersRatingInteractor(final ExecutorService executor, final IRatingRepository ratingRepository) {
        super(executor);
        mRatingRepository = ratingRepository;
    }

    @Override
    protected ProvidersRating buildUseCase(final Boolean aBoolean) throws Throwable {
        return mRatingRepository.getProvidersRating();
    }
}
