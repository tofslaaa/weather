package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IYandexWeatherRepository;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.Weather;

import java.util.concurrent.ExecutorService;

public class RequestYandexCurrentInteractor extends RequestInteractor<Coordinates, Weather> {

    private final IYandexWeatherRepository mYandexWeatherRepository;

    public RequestYandexCurrentInteractor(
            final ExecutorService executor,
            final IYandexWeatherRepository yandexWeatherRepository) {
        super(executor);
        mYandexWeatherRepository = yandexWeatherRepository;
    }

    @Override
    protected Weather buildUseCase(final Coordinates params) throws Throwable {
        return mYandexWeatherRepository.getCurrentWeather(params.getLon(), params.getLat());
    }

}
