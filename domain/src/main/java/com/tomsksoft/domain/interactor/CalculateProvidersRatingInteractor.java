package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IRatingRepository;
import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.Provider;
import com.tomsksoft.domain.model.ProvidersRating;
import com.tomsksoft.domain.model.UserReport;
import com.tomsksoft.domain.model.Weather;
import com.tomsksoft.domain.model.WindType;

import java.util.Map;
import java.util.concurrent.ExecutorService;

import static com.tomsksoft.domain.interactor.CalculateProvidersRatingInteractor.Params;

public class CalculateProvidersRatingInteractor extends RequestInteractor<Params, ProvidersRating> {

    private final IRatingRepository mRatingRepository;

    public CalculateProvidersRatingInteractor(final ExecutorService executor,
                                              final IRatingRepository ratingRepository) {
        super(executor);
        mRatingRepository = ratingRepository;
    }

    @Override
    protected ProvidersRating buildUseCase(final Params params) throws Throwable {
        final ProvidersRating rating = mRatingRepository.getProvidersRating();
        final Map<Provider, Weather> weathers = params.getWeathers();

        for (final Map.Entry<Provider, Weather> entry : weathers.entrySet()) {
            final Weather weather = entry.getValue();
            final WindType windType = getWindType(weather.getWindSpeed());
            final WeatherPoint weatherPoint = createWeatherPoint(weather.getCloudiness(), weather.getTemperature(), windType);

            final UserReport report = params.getUserReport();
            final WeatherPoint userPoint = createWeatherPoint(report.getCloudiness(), report.getTemperature(), report.getWind());

            final double diff = calculateDiff(weatherPoint, userPoint);
            rating.addDiff(entry.getKey(), diff);
        }
        mRatingRepository.saveProvidersRating(rating);
        return rating;
    }

    private double calculateDiff(final WeatherPoint weatherPoint, final WeatherPoint userPoint) {
        final double usersCloudiness = (userPoint.cloudinessPoint / 6.0);
        final double providersCloudiness = (weatherPoint.cloudinessPoint / 6.0);

        final double usersWind = (userPoint.windPoint / 4.0);
        final double providersWind = (userPoint.windPoint / 4.0);

        final double usersTemperature;
        if (userPoint.temperature / 0 == Double.POSITIVE_INFINITY) {
            usersTemperature = (userPoint.temperature + 60) / 120.0;
        } else {
            usersTemperature = Math.abs(userPoint.temperature) / 120.0;
        }

        final double providersTemperature;
        if (weatherPoint.temperature / 0 == Double.POSITIVE_INFINITY) {
            providersTemperature = (weatherPoint.temperature + 60) / 120.0;
        } else {
            providersTemperature = Math.abs(weatherPoint.temperature) / 120.0;
        }

        return Math.sqrt(Math.pow((usersCloudiness - providersCloudiness), 2) +
                         Math.pow(((4.0 / 6.0) * (usersWind - providersWind)), 2) +
                         Math.pow(((120.0 / 6.0) * (usersTemperature - providersTemperature)), 2));
    }

    private WeatherPoint createWeatherPoint(final Cloudiness cloudiness, final double temperature, final WindType windType) {
        final double cloudinessPoint = getCloudinessPoint(cloudiness);
        final double windPoint = getWindPoints(windType);
        return new WeatherPoint(cloudinessPoint, windPoint, temperature);
    }

    private WindType getWindType(final double windSpeed) {
        if (windSpeed >= 0 && windSpeed <= 1) {
            return WindType.CALM;
        } else if (windSpeed <= 5) {
            return WindType.LIGHT;
        } else if (windSpeed <= 10) {
            return WindType.MODERATE;
        } else if (windSpeed > 10) {
            return WindType.STRONG;
        } else {
            return WindType.NONE;
        }
    }

    private double getWindPoints(final WindType wind) {
        switch (wind) {
            case CALM:
                return 1;
            case LIGHT:
                return 2;
            case MODERATE:
                return 3;
            case STRONG:
                return 4;
            case NONE:
            default:
                return 0;
        }
    }

    private double getCloudinessPoint(final Cloudiness cloudiness) {
        switch (cloudiness) {
            case CLEAR:
                return 1;
            case CLOUDY:
                return 2;
            case OVERCAST:
                return 3;
            case RAIN:
                return 4;
            case SLEET:
                return 5;
            case SNOW:
                return 6;
            case NONE:
            default:
                return Double.NaN;
        }
    }

    private class WeatherPoint {

        private final double cloudinessPoint;
        private final double windPoint;
        private final double temperature;

        public WeatherPoint(final double cloudinessPoint, final double windPoint, final double temperature) {

            this.cloudinessPoint = cloudinessPoint;
            this.windPoint = windPoint;
            this.temperature = temperature;
        }
    }


    public static class Params {

        private final Map<Provider, Weather> mWeathers;
        private final UserReport mUsersData;

        public Params(final UserReport userReport, final Map<Provider, Weather> weathers) {
            mWeathers = weathers;
            mUsersData = userReport;
        }

        public Map<Provider, Weather> getWeathers() {
            return mWeathers;
        }

        public UserReport getUserReport() {
            return mUsersData;
        }
    }
}
