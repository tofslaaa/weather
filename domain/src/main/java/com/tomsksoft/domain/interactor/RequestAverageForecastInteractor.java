package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.model.AverageForecast;
import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.Forecasts;
import com.tomsksoft.domain.model.Forecasts.Forecast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static com.tomsksoft.domain.interactor.RequestAverageForecastInteractor.Params;

public class RequestAverageForecastInteractor extends RequestInteractor<Params, List<AverageForecast>> {

    public RequestAverageForecastInteractor(final ExecutorService executor) {
        super(executor);
    }

    @Override
    protected List<AverageForecast> buildUseCase(final Params params) throws Throwable {
        final Forecasts[] forecasts = params.getForecasts();
        if (forecasts.length == 0) {
            throw new IllegalArgumentException("at least one forecast required");
        }

        final Map<Long, AverageForecast> valuesForecast = new HashMap<>();
        final Map<Long, Integer> countForecast = new HashMap<>();
        final List<AverageForecast> resultForecasts = new ArrayList<>();

        final Map<Long, CloudinessParams> averageCloudinessMap = new HashMap<>();

        for (final Forecasts forecast : forecasts) {
            for (final Map.Entry<Long, Forecast> forecastEntry : forecast.getForecasts().entrySet()) {
                final long millis = forecastEntry.getValue().getMillis();
                final Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(millis);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                final long currentMillis = calendar.getTimeInMillis();

                final Integer countOfProviders = countForecast.get(currentMillis);
                countForecast.put(currentMillis, countOfProviders == null
                                                 ? 1
                                                 : countOfProviders + 1);

                final double currentTemperature = forecastEntry.getValue().getWeather().getTemperature();
                final AverageForecast averageForecast = valuesForecast.get(currentMillis);
                final double averageTemperature = averageForecast == null
                                                  ? currentTemperature
                                                  : averageForecast.getTemperature() + currentTemperature;

                final Cloudiness currentCloudiness = forecastEntry.getValue().getWeather().getCloudiness();
                final CloudinessParams cloudinessParams = averageCloudinessMap.get(currentMillis);
                if (cloudinessParams == null) {
                    averageCloudinessMap.put(currentMillis, new CloudinessParams(currentCloudiness, 1));
                } else {
                    final Integer count = cloudinessParams.getAverageMap().get(currentCloudiness);
                    if (count == null) {
                        averageCloudinessMap.put(currentMillis, new CloudinessParams(currentCloudiness, 1));
                    } else {
                        averageCloudinessMap.put(currentMillis, new CloudinessParams(currentCloudiness, count + 1));
                    }
                }

                valuesForecast.put(
                        currentMillis,
                        new AverageForecast(currentMillis, averageTemperature, Cloudiness.NONE)
                );
            }
        }

        for (final Map.Entry<Long, Integer> countEntry : countForecast.entrySet()) {
            if (countEntry.getValue() == forecasts.length) {
                final long millis = countEntry.getKey();
                final double temperature = valuesForecast.get(millis).getTemperature() / forecasts.length;

                int maxCountCloudiness = 0;
                Cloudiness maxValueCloudiness = Cloudiness.NONE;

                for (final Map.Entry<Cloudiness, Integer> cloudinessEntry : averageCloudinessMap.get(millis).getAverageMap().entrySet()) {
                    final Integer count = cloudinessEntry.getValue();
                    if (count >= maxCountCloudiness) {
                        maxCountCloudiness = count;
                        maxValueCloudiness = cloudinessEntry.getKey();
                    }
                }
                resultForecasts.add(new AverageForecast(millis, temperature, maxValueCloudiness));
            }
        }

        Collections.sort(resultForecasts, (o1, o2) -> (int) (o1.getMillis() - o2.getMillis()));
        return resultForecasts;
    }

    public static class CloudinessParams {

        private final Map<Cloudiness, Integer> averageMap = new HashMap<>();

        public CloudinessParams(final Cloudiness cloudiness, final Integer count) {
            averageMap.put(cloudiness, count);
        }

        public Map<Cloudiness, Integer> getAverageMap() {
            return averageMap;
        }
    }

    public static class Params {

        private final Forecasts[] mForecasts;

        public Params(final Forecasts... forecasts) {mForecasts = forecasts;}

        public Forecasts[] getForecasts() {
            return mForecasts;
        }
    }
}
