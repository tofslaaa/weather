package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IYandexWeatherRepository;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.Forecasts;

import java.util.concurrent.ExecutorService;

public class RequestYandexForecastInteractor extends RequestInteractor<Coordinates, Forecasts> {

    private final IYandexWeatherRepository mYandexWeatherRepository;

    public RequestYandexForecastInteractor(final ExecutorService executor,
                                           final IYandexWeatherRepository yandexWeatherRepository) {
        super(executor);
        mYandexWeatherRepository = yandexWeatherRepository;
    }

    @Override
    protected Forecasts buildUseCase(final Coordinates params) throws Throwable {
        return mYandexWeatherRepository.getForecasts(params.getLon(), params.getLat());
    }
}
