package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IYahooWeatherRepository;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.Weather;

import java.util.concurrent.ExecutorService;

public class RequestYahooCurrentInteractor extends RequestInteractor<Coordinates, Weather> {

    private final IYahooWeatherRepository mYahooWeatherRepository;

    public RequestYahooCurrentInteractor(final ExecutorService executorService, final IYahooWeatherRepository yahooWeatherRepository) {
        super(executorService);
        mYahooWeatherRepository = yahooWeatherRepository;
    }

    @Override
    protected Weather buildUseCase(final Coordinates coordinates) throws Throwable {
        return mYahooWeatherRepository.getCurrentWeather(coordinates.getLon(), coordinates.getLat());
    }
}
