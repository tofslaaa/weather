package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.model.AverageWeather;
import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.Weather;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static com.tomsksoft.domain.interactor.RequestAverageWeatherInteractor.Params;

public class RequestAverageWeatherInteractor extends RequestInteractor<Params, AverageWeather> {

    public RequestAverageWeatherInteractor(final ExecutorService executor) {
        super(executor);
    }

    @Override
    protected AverageWeather buildUseCase(final Params params) throws Throwable {
        final Weather[] weathers = params.getWeathers();
        if (weathers.length == 0) {
            throw new IllegalArgumentException("at least one weather required");
        }

        double temperature = 0.0;
        double windSpeed = 0.0;
        double pressure = 0.0;
        double humidity = 0.0;
        double windDirection = 0.0;
        final Map<Cloudiness, Integer> averageMap = new HashMap<>();

        int maxCount = 0;
        Cloudiness maxCloudiness = Cloudiness.NONE;
        for (final Weather weather : weathers) {
            temperature += weather.getTemperature();
            windSpeed += weather.getWindSpeed();
            windDirection += weather.getWindDirection();
            pressure += weather.getPressure();
            humidity += weather.getHumidity();
            final Cloudiness cloudiness = weather.getCloudiness();
            final Integer previousCount = averageMap.get(cloudiness);
            final int count = previousCount == null ? 1 : previousCount + 1;
            if (count >= maxCount) {
                maxCount = count;
                maxCloudiness = cloudiness;
            }
            averageMap.put(cloudiness, count);
        }

        temperature /= weathers.length;
        windSpeed /= weathers.length;
        windDirection /= weathers.length;
        pressure /= weathers.length;
        humidity /= weathers.length;

        return new AverageWeather(temperature, windSpeed, windDirection, pressure, humidity, maxCloudiness);
    }

    public static class Params {

        private final Weather[] mWeathers;

        public Params(final Weather... weathers) {
            mWeathers = weathers;
        }

        public Weather[] getWeathers() {
            return mWeathers;
        }
    }
}
