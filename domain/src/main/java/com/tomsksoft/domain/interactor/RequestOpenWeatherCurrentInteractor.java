package com.tomsksoft.domain.interactor;

import com.tomsksoft.domain.base.interactor.RequestInteractor;
import com.tomsksoft.domain.base.repository.IOpenWeatherRepository;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.Weather;

import java.util.concurrent.ExecutorService;

public class RequestOpenWeatherCurrentInteractor extends RequestInteractor<Coordinates, Weather> {

    private final IOpenWeatherRepository mOpenWeatherRepository;

    public RequestOpenWeatherCurrentInteractor(
            final ExecutorService executor,
            final IOpenWeatherRepository openWeatherRepository) {
        super(executor);
        mOpenWeatherRepository = openWeatherRepository;
    }

    @Override
    protected Weather buildUseCase(final Coordinates params) throws Throwable {
        return mOpenWeatherRepository.getCurrentWeather(params.getLon(), params.getLat());
    }
}
