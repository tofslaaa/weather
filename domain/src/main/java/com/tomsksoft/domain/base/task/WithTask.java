package com.tomsksoft.domain.base.task;

import java.util.concurrent.ExecutorService;

public class WithTask<L1, L2, Result> extends BaseTask<Result> {

    private final BaseTask<L1> mFrom;
    private final BaseTask<L2> mWith;
    private final Function2<L1, L2, Result> mFunction;
    private L1 mResult1;
    private L2 mResult2;

    public WithTask(final ExecutorService executor,
                    final BaseTask<L1> from,
                    final BaseTask<L2> with,
                    final Function2<L1, L2, Result> function) {
        super(executor);
        mFrom = from;
        mWith = with;
        mFunction = function;
    }


    @Override
    public Task submit(final Callback<Result> callback) {
        return new CompositeTask(mFrom.listen(l1 -> {
            mResult1 = l1;
            await(callback);
        }, callback::onError), mWith.listen(l2 -> {
            mResult2 = l2;
            await(callback);
        }, callback::onError));
    }

    private void await(final Callback<Result> callback) {
        if (mResult1 != null && mResult2 != null) {
            try {
                callback.onResult(mFunction.apply(mResult1, mResult2));
            } catch (final Throwable throwable) {
                callback.onError(throwable);
            }
        }
    }

    @Override
    protected void clear() {
        super.clear();
        mFrom.clear();
        mWith.clear();
    }
}
