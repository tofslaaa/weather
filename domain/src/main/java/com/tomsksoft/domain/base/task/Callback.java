package com.tomsksoft.domain.base.task;

public interface Callback<Result> {

    void onResult(Result result);

    void onError(final Throwable throwable);
}
