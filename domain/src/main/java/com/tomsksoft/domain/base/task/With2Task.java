package com.tomsksoft.domain.base.task;

import java.util.concurrent.ExecutorService;

public class With2Task<L1, L2, L3, Result> extends BaseTask<Result> {

    private final BaseTask<L1> mFrom;
    private final BaseTask<L2> mWith1;
    private final BaseTask<L3> mWith2;
    private final Function3<L1, L2, L3, Result> mFunction3;
    private L1 mResult1;
    private L2 mResult2;
    private L3 mResult3;

    public With2Task(final ExecutorService executor,
                     final BaseTask<L1> from,
                     final BaseTask<L2> with1,
                     final BaseTask<L3> with2,
                     final Function3<L1, L2, L3, Result> function) {
        super(executor);
        mFrom = from;
        mWith1 = with1;
        mWith2 = with2;
        mFunction3 = function;
    }

    @Override
    public Task submit(final Callback<Result> callback) {
        return new CompositeTask(mFrom.listen(l1 -> {
            mResult1 = l1;
            await(callback);
        }, callback::onError), mWith1.listen(l2 -> {
            mResult2 = l2;
            await(callback);
        }, callback::onError), mWith2.listen(l3 -> {
            mResult3 = l3;
            await(callback);
        }, callback::onError));
    }

    private void await(final Callback<Result> callback) {
        if (mResult1 != null && mResult2 != null && mResult3 != null) {
            try {
                callback.onResult(mFunction3.apply(mResult1, mResult2, mResult3));
            } catch (final Throwable throwable) {
                callback.onError(throwable);
            }
        }
    }

    @Override
    protected void clear() {
        super.clear();
        mFrom.clear();
        mWith1.clear();
        mWith2.clear();
    }
}