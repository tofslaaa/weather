package com.tomsksoft.domain.base.repository;

import com.tomsksoft.domain.model.Coordinates;

public interface ICoordinatesRepository extends BaseRepository {

    Coordinates getLastCoordinates();

    void saveLastCoordinates(double longitude, double latitude);
}
