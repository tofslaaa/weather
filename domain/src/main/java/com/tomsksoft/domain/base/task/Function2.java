package com.tomsksoft.domain.base.task;

public interface Function2<L1, L2, R> {

    R apply(L1 l1, L2 l2) throws Throwable;

}
