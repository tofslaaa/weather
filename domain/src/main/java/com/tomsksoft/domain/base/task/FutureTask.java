package com.tomsksoft.domain.base.task;

import java.util.concurrent.Future;

public class FutureTask implements Task {

    private final Future<?> mFuture;

    public FutureTask(final Future<?> future) {
        mFuture = future;
    }

    @Override
    public void cancel() {
        mFuture.cancel(true);
    }
}
