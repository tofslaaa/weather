package com.tomsksoft.domain.base.repository;

import com.tomsksoft.domain.model.ProvidersRating;

public interface IRatingRepository {

    ProvidersRating getProvidersRating();

    void saveProvidersRating(ProvidersRating providersRating);

}
