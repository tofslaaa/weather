package com.tomsksoft.domain.base.task;

public interface Listener<T> {

    void onResult(T t);

}
