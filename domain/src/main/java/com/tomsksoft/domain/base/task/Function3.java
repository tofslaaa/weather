package com.tomsksoft.domain.base.task;

public interface Function3<L1, L2, L3, R> {

    R apply(L1 l1, L2 l2, L3 l3) throws Throwable;
}
