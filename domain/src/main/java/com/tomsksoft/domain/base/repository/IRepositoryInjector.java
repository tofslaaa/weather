package com.tomsksoft.domain.base.repository;

public interface IRepositoryInjector extends BaseRepository {

    IOpenWeatherRepository getOpenWeatherRepository();

    IYandexWeatherRepository getYandexWeatherRepository();

    IYahooWeatherRepository getYahooWeatherRepository();

    IRatingRepository getRatingRepository();

    ICoordinatesRepository getCoordinatesRepository();
}
