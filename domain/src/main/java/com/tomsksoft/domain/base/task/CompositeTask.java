package com.tomsksoft.domain.base.task;

public class CompositeTask implements Task {

    private final Task[] mTasks;

    public CompositeTask(final Task... tasks) {
        mTasks = tasks;
    }

    @Override
    public void cancel() {
        for (final Task task : mTasks) {
            task.cancel();
        }
    }
}
