package com.tomsksoft.domain.base.repository;

import com.tomsksoft.domain.model.Weather;

public interface IYahooWeatherRepository extends BaseRepository {

    Weather getCurrentWeather(final double lon, final double lat) throws Throwable;

}
