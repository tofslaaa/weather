package com.tomsksoft.domain.base.task;

public interface Function<L, R> {

    R apply(L l);
}
