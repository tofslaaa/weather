package com.tomsksoft.domain.base.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public abstract class BaseTask<Result> implements Task {

    protected boolean canceled;

    protected final ExecutorService mExecutor;

    private final List<Task> mTasks = new ArrayList<>();

    public BaseTask(final ExecutorService executor) {
        mExecutor = executor;
    }

    public Task listen(final Callback<Result> callback) {
        final Task task = canceled
                          ? new EmptyTask()
                          : submit(callback);
        mTasks.add(task);
        return task;
    }

    abstract Task submit(final Callback<Result> callback);

    public Task listen(final Listener<Result> onResult) {
        return listen(onResult, e -> {
            throw new NotImplementedException();
        });
    }

    public Task listen(final Listener<Result> onResult, final Listener<Throwable> onError) {
        final Callback<Result> callback = new Callback<Result>() {

            @Override
            public void onResult(final Result result) {
                if (!canceled) {
                    onResult.onResult(result);
                }
            }

            @Override
            public void onError(final Throwable throwable) {
                if (!canceled) {
                    onError.onResult(throwable);
                }
            }
        };

        return listen(callback);
    }

    public <R, T> BaseTask<T> with(final BaseTask<R> with, final Function2<Result, R, T> function) {
        return new WithTask<>(mExecutor, this, with, function);
    }

    public <R, Q, T> BaseTask<T> with(final BaseTask<R> with1, final BaseTask<Q> with2, final Function3<Result, R, Q, T> function) {
        return new With2Task<>(mExecutor, this, with1, with2, function);
    }

    public <T> BaseTask<T> andThen(final Function<Result, BaseTask<T>> function) {
        return new ThenTask<>(mExecutor, this, function);
    }

    @Override
    public void cancel() {
        if (!canceled) {
            canceled = true;
            for (final Task task : mTasks) {
                task.cancel();
            }
            clear();
        }
    }

    protected void clear() { }
}
