package com.tomsksoft.domain.base.task;

import java.util.concurrent.ExecutorService;

class ThenTask<L, R> extends BaseTask<R> {

    private final BaseTask<L> mFrom;
    private final Function<L, BaseTask<R>> mFunction;

    public ThenTask(final ExecutorService executor,
                    final BaseTask<L> from,
                    final Function<L, BaseTask<R>> function) {
        super(executor);
        mFrom = from;
        mFunction = function;
    }

    @Override
    Task submit(final Callback<R> callback) {
        return mFrom.listen(new Callback<L>() {

            @Override
            public void onResult(final L l) {
                if (!canceled) {
                    mFunction.apply(l).listen(callback);
                }
            }

            @Override
            public void onError(final Throwable throwable) {
                if (!canceled) {
                    callback.onError(throwable);
                }
            }
        });
    }
}
