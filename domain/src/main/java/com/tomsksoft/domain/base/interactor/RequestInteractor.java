package com.tomsksoft.domain.base.interactor;

import com.tomsksoft.domain.base.task.BaseTask;
import com.tomsksoft.domain.base.task.RequestTask;

import java.util.concurrent.ExecutorService;

public abstract class RequestInteractor<Params, Result> implements BaseInteractor {

    private final ExecutorService mExecutor;

    protected RequestInteractor(final ExecutorService executor) {
        mExecutor = executor;
    }

    protected abstract Result buildUseCase(final Params params) throws Throwable;

    public final BaseTask<Result> request(final Params params) {
        return new RequestTask<>(mExecutor, () -> call(params));
    }

    private Result call(final Params params) throws Throwable {
        final Result result = buildUseCase(params);
        if (result == null) {
            throw new NullPointerException("Result is null");
        }
        return result;
    }
}
