package com.tomsksoft.domain.base.task;

import java.util.concurrent.ExecutorService;

public class RequestTask<Result> extends BaseTask<Result> {

    private Supplier<Result> mSupplier;

    private Result mResult;
    private Throwable mThrowable;

    public RequestTask(final ExecutorService executor, final Supplier<Result> supplier) {
        super(executor);
        mSupplier = supplier;
    }

    @Override
    public Task submit(final Callback<Result> callback) {
        return new FutureTask(mExecutor.submit(() -> {
            try {
                if (mThrowable != null) {
                    callback.onError(mThrowable);
                    return;
                }
                if (mResult == null) {
                    mResult = mSupplier.get();
                }
                if (mResult == null) {
                    throw new NullPointerException("Result is null");
                }

                callback.onResult(mResult);
            } catch (final Throwable throwable) {
                mThrowable = throwable;
                callback.onError(mThrowable);
                mSupplier = null;
            }
        }));
    }

    @Override
    protected void clear() {
        mSupplier = null;
    }


}
