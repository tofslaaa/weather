package com.tomsksoft.domain.base.repository;

import com.tomsksoft.domain.model.Forecasts;
import com.tomsksoft.domain.model.Weather;

public interface IOpenWeatherRepository extends BaseRepository {

    Weather getCurrentWeather(double lon, double lat) throws Throwable;

    Forecasts getForecasts(double lon, double lat) throws Throwable;
}
