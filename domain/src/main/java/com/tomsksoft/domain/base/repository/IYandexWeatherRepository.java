package com.tomsksoft.domain.base.repository;

import com.tomsksoft.domain.model.Forecasts;
import com.tomsksoft.domain.model.Weather;

public interface IYandexWeatherRepository extends BaseRepository {

    Weather getCurrentWeather(final double lon, final double lat) throws Throwable;

    Forecasts getForecasts(final double lon, final double lat) throws Throwable;

    Weather getCurrentWeatherShort(double lon, double lat) throws Throwable;

    Forecasts getForecastsShort(double lon, double lat) throws Throwable;
}
