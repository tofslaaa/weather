package com.tomsksoft.domain.base.task;

public interface Supplier<T> {

    T get() throws Throwable;
}
