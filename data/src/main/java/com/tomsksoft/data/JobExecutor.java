package com.tomsksoft.data;

import androidx.annotation.NonNull;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class JobExecutor extends ThreadPoolExecutor {

    public JobExecutor(final int corePoolSize,
                       final int maximumPoolSize,
                       final long keepAliveTime,
                       final TimeUnit unit) {
        super(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                new LinkedBlockingQueue<>(),
                new ExecutorThreadFactory()
        );
    }

    private static class ExecutorThreadFactory implements ThreadFactory {

        private int counter = 0;

        @Override
        public Thread newThread(@NonNull final Runnable runnable) {
            return new Thread(runnable, "android_" + counter++);
        }
    }
}
