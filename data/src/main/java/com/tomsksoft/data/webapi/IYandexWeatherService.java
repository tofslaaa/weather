package com.tomsksoft.data.webapi;

import com.tomsksoft.data.model.YandexWeatherModel;
import com.tomsksoft.data.model.YandexWeatherModelShort;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface IYandexWeatherService {

    String BASE_URL = "https://api.weather.yandex.ru";
    String API_KEY = "dfcb5581-6b48-4d77-8e8a-321075f7e06a";

    @GET(BASE_URL + "/v1/forecast")
    @Headers("X-Yandex-API-Key: " + API_KEY)
    Call<YandexWeatherModel> getCurrentByCoord(@Query("lon") double lon, @Query("lat") double lat);

    @GET(BASE_URL + "/v1/informers")
    @Headers("X-Yandex-API-Key: " + API_KEY)
    Call<YandexWeatherModelShort> getCurrentByCoordShort(@Query("lon") double lon, @Query("lat") double lat);

}
