package com.tomsksoft.data.webapi;

import com.tomsksoft.data.model.YahooWeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IYahooWeatherService {

    String BASE_URL = "https://weather-ydn-yql.media.yahoo.com";

    @GET(BASE_URL + "/forecastrss")
    Call<YahooWeatherModel> getCurrentByCoord(@Query("lon") double lon,
                                              @Query("lat") double lat,
                                              @Query("format") String format,
                                              @Query("u") String u);
}
