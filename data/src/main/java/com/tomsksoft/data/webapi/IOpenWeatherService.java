package com.tomsksoft.data.webapi;

import com.tomsksoft.data.model.CurrentOpenWeatherModel;
import com.tomsksoft.data.model.ForecastOpenWeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IOpenWeatherService {

    String BASE_URL = "https://api.openweathermap.org";
    String API_KEY = "04d4bb50535fe7776daa1ccbb432f0eb";

    @GET(BASE_URL + "/data/2.5/weather?appid=" + API_KEY + "&units=metric")
    Call<CurrentOpenWeatherModel> getCurrentByCoord(@Query("lon") double lon, @Query("lat") double lat);

    @GET(BASE_URL + "/data/2.5/forecast?appid=" + API_KEY + "&units=metric")
    Call<ForecastOpenWeatherModel> getForecastByCoord(@Query("lon") double lon, @Query("lat") double lat);

}
