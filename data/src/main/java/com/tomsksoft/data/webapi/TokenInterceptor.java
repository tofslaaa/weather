package com.tomsksoft.data.webapi;

import android.util.Log;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;

import java.io.IOException;
import java.net.URISyntaxException;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    private static final String TAG = TokenInterceptor.class.getSimpleName();
    private final static String APP_ID = "FO0B5l4s";
    private final static String CONSUMER_KEY = "dj0yJmk9QzlXQnhZS2xrTGdXJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWZl";
    private final static String CONSUMER_SECRET = "af117a391dca12c5d939d03e6c719100be805b62";

    @Override
    public Response intercept(final Chain chain) throws IOException {
        // Add new header to rejected request and retry it
        final Request request = chain.request();
        final String url = request.url().url().toString();
        try {
            return chain.proceed(request.newBuilder().headers(getHeaders(url)).build());
        } catch (final Exception e) {
            Log.e(TAG, "get headers error: ", e);
            return null;
        }
    }

    private Headers getHeaders(final String url) throws OAuthException, IOException, URISyntaxException {
        final Headers.Builder builder = new Headers.Builder();
        final OAuthConsumer consumer = new OAuthConsumer(null, CONSUMER_KEY, CONSUMER_SECRET, null);
        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.HMAC_SHA1);
        final OAuthAccessor accessor = new OAuthAccessor(consumer);

        final OAuthMessage request = accessor.newRequestMessage(OAuthMessage.GET, url, null);
        final String authorization = request.getAuthorizationHeader(null);
        builder.add("Authorization", authorization);

        builder.add("X-Yahoo-App-Id", APP_ID);
        builder.add("Content-Type", "application/json");
        return builder.build();
    }
}
