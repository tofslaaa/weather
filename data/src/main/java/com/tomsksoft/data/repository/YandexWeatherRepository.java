package com.tomsksoft.data.repository;

import com.tomsksoft.data.model.YandexWeatherModel;
import com.tomsksoft.data.model.YandexWeatherModelShort;
import com.tomsksoft.data.webapi.IYandexWeatherService;
import com.tomsksoft.domain.base.repository.IYandexWeatherRepository;
import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.Forecasts;
import com.tomsksoft.domain.model.Forecasts.Forecast;
import com.tomsksoft.domain.model.Weather;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.tomsksoft.domain.model.Cloudiness.CLEAR;
import static com.tomsksoft.domain.model.Cloudiness.CLOUDY;
import static com.tomsksoft.domain.model.Cloudiness.NONE;
import static com.tomsksoft.domain.model.Cloudiness.OVERCAST;
import static com.tomsksoft.domain.model.Cloudiness.RAIN;
import static com.tomsksoft.domain.model.Cloudiness.SLEET;
import static com.tomsksoft.domain.model.Cloudiness.SNOW;

public class YandexWeatherRepository implements IYandexWeatherRepository {

    private final IYandexWeatherService mService;

    public YandexWeatherRepository(final IYandexWeatherService service) {
        mService = service;
    }

    @Override
    public Weather getCurrentWeather(final double lon, final double lat) throws Throwable {
        final YandexWeatherModel model = sendWeatherRequest(lon, lat);
        final YandexWeatherModel.Fact fact = model.getFact();
        return new Weather(
                fact.getTemp(),
                model.getInfo().getTzinfo().getName(),
                fact.getWindSpeed(),
                mapWindDirection(fact.getWindDirection()),
                fact.getPressureMm(),
                fact.getHumidity(),
                mapCloudiness(fact.getCloudiness())
        );
    }

    @Override
    public Forecasts getForecasts(final double lon, final double lat) throws Throwable {
        final YandexWeatherModel model = sendWeatherRequest(lon, lat);
        final List<Forecast> forecasts = new ArrayList<>(24);
        for (final YandexWeatherModel.Forecast forecast : model.getForecasts()) {
            for (final YandexWeatherModel.Hour hour : forecast.getHours()) {
                final long hourMillis = hour.getHourTs() * 1000;
                forecasts.add(new Forecast(
                        hourMillis,
                        new Weather(
                                hour.getTemp(),
                                model.getInfo().getTzinfo().getName(),
                                hour.getWindSpeed(),
                                mapWindDirection(hour.getWindDir()),
                                hour.getPressureMm(),
                                hour.getHumidity(),
                                mapCloudiness(hour.getCondition())
                        )
                ));
            }
        }
        return new Forecasts(forecasts);
    }

    private YandexWeatherModel sendWeatherRequest(final double lon, final double lat) throws IOException {
        final Call<YandexWeatherModel> call = mService.getCurrentByCoord(lon, lat);
        final Response<YandexWeatherModel> response = call.execute();

        if (response.isSuccessful()) {
            final YandexWeatherModel body = response.body();
            if (body != null) {
                return body;
            } else {
                throw new RuntimeException("response body null");
            }
        } else {
            throw new RuntimeException(response.errorBody().string());
        }

    }

    @Override
    public Weather getCurrentWeatherShort(final double lon, final double lat) throws Throwable {
        final YandexWeatherModelShort model = sendWeatherShortRequest(lon, lat);
        final YandexWeatherModelShort.Fact fact = model.getFact();
        return new Weather(
                fact.getTemp(),
                "",
                fact.getWindSpeed(),
                mapWindDirection(fact.getWindDir()),
                fact.getPressureMm(),
                fact.getHumidity(),
                mapCloudiness(fact.getCondition())
        );
    }

    @Override
    public Forecasts getForecastsShort(final double lon, final double lat) throws Throwable {
        final YandexWeatherModelShort model = sendWeatherShortRequest(lon, lat);
        final List<Forecast> forecasts = new ArrayList<>(24);
        final YandexWeatherModelShort.Forecast forecast = model.getForecast();
        final YandexWeatherModelShort.Fact fact = model.getFact();
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(calendar.getTimeInMillis());
        final long nowMillisLocale = calendar.getTimeInMillis();
        forecasts.add(new Forecast(
                nowMillisLocale,
                new Weather(
                        fact.getTemp(),
                        "",
                        fact.getWindSpeed(),
                        mapWindDirection(fact.getWindDir()),
                        fact.getPressureMm(),
                        fact.getHumidity(),
                        mapCloudiness(fact.getCondition())
                )
        ));
        for (int i = 0; i < forecast.getParts().size(); i++) {
            final YandexWeatherModelShort.Part part = forecast.getParts().get(i);
            final long millis = nowMillisLocale + (i + 1) * 1000 * 60 * 60 * 6;
            if (millis >= 0) {
                forecasts.add(new Forecast(
                        millis,
                        new Weather(
                                part.getTempAvg(),
                                "",
                                part.getWindSpeed(),
                                mapWindDirection(part.getWindDir()),
                                part.getPressureMm(),
                                part.getHumidity(),
                                mapCloudiness(part.getCondition())
                        )
                ));
            }
        }
        return new Forecasts(forecasts);
    }

    private YandexWeatherModelShort sendWeatherShortRequest(final double lon, final double lat) throws IOException {
        final Call<YandexWeatherModelShort> call = mService.getCurrentByCoordShort(lon, lat);
        final Response<YandexWeatherModelShort> response = call.execute();

        if (response.isSuccessful()) {
            final YandexWeatherModelShort body = response.body();
            if (body != null) {
                return body;
            } else {
                throw new RuntimeException("response body null");
            }
        } else {
            throw new RuntimeException(response.errorBody().string());
        }

    }

    private double mapWindDirection(final String windDirection) {
        switch (windDirection.toLowerCase()) {
            case "n":
                return 0;
            case "ne":
                return 45;
            case "e":
                return 90;
            case "se":
                return 135;
            case "s":
                return 180;
            case "sw":
                return 225;
            case "w":
                return 270;
            case "nw":
                return 315;
            default:
                return 0;
        }

    }

    private Cloudiness mapCloudiness(final String cloudiness) {
        switch (cloudiness) {
            case "clear":
                return CLEAR;
            case "partly-cloudy":
                return CLOUDY;
            case "cloudy":
                return CLOUDY;
            case "overcast":
                return OVERCAST;
            case "partly-cloudy-and-light-rain":
                return RAIN;
            case "partly-cloudy-and-rain":
                return RAIN;
            case "overcast-and-rain":
                return RAIN;
            case "overcast-thunderstorms-with-rain":
                return RAIN;
            case "cloudy-and-light-rain":
                return RAIN;
            case "overcast-and-light-rain":
                return RAIN;
            case "cloudy-and-rain":
                return RAIN;
            case "overcast-and-wet-snow":
                return SLEET;
            case "partly-cloudy-and-light-snow":
                return SNOW;
            case "partly-cloudy-and-snow":
                return SNOW;
            case "overcast-and-snow":
                return SNOW;
            case "cloudy-and-light-snow":
                return SNOW;
            case "overcast-and-light-snow":
                return SNOW;
            case "cloudy-and-snow":
                return SNOW;
            default:
                return NONE;
        }
    }
}
