package com.tomsksoft.data.repository;

import com.tomsksoft.data.model.CurrentOpenWeatherModel;
import com.tomsksoft.data.model.ForecastOpenWeatherModel;
import com.tomsksoft.data.webapi.IOpenWeatherService;
import com.tomsksoft.domain.base.repository.IOpenWeatherRepository;
import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.Forecasts;
import com.tomsksoft.domain.model.Forecasts.Forecast;
import com.tomsksoft.domain.model.Weather;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.tomsksoft.domain.model.Cloudiness.CLEAR;
import static com.tomsksoft.domain.model.Cloudiness.CLOUDY;
import static com.tomsksoft.domain.model.Cloudiness.NONE;
import static com.tomsksoft.domain.model.Cloudiness.OVERCAST;
import static com.tomsksoft.domain.model.Cloudiness.RAIN;
import static com.tomsksoft.domain.model.Cloudiness.SLEET;
import static com.tomsksoft.domain.model.Cloudiness.SNOW;

public class OpenWeatherRepository implements IOpenWeatherRepository {

    private final IOpenWeatherService mService;

    public OpenWeatherRepository(final IOpenWeatherService service) {
        mService = service;
    }

    @Override
    public Weather getCurrentWeather(final double lon, final double lat) throws Throwable {
        final CurrentOpenWeatherModel body = sendRequest(mService.getCurrentByCoord(lon, lat));
        return new Weather(
                body.getMain().getTemp(),
                body.getName(),
                body.getWind().getSpeed(),
                body.getWind().getDeg(), body.getMain().getPressure() / 1.333,
                body.getMain().getHumidity(),
                mapCloudiness(body.getWeather().get(0).getId())
        );
    }

    @Override
    public Forecasts getForecasts(final double lon, final double lat) throws Throwable {
        final ForecastOpenWeatherModel response = sendRequest(mService.getForecastByCoord(lon, lat));
        final List<Forecast> forecasts = new ArrayList<>(8); //initial size: response.getData().size()
        for (final ForecastOpenWeatherModel.Data data : response.getData()) {
            final long millis = data.getTimeSeconds() * 1000;
            forecasts.add(new Forecast(
                    millis,
                    new Weather(
                            data.getMain().getTemp(),
                            response.getCity().getName(),
                            data.getWind().getSpeed(),
                            data.getWind().getDeg(), data.getMain().getPressure() / 1.333,
                            data.getMain().getHumidity(),
                            mapCloudiness(data.getWeather().get(0).getId())
                    )
            ));
        }
        return new Forecasts(forecasts);
    }

    private <T> T sendRequest(final Call<T> call) throws IOException {
        final Response<T> response = call.execute();
        if (response.isSuccessful()) {
            final T body = response.body();
            if (body != null) {
                return body;
            } else {
                throw new RuntimeException("response body null");
            }
        } else {
            throw new RuntimeException(response.errorBody().string());
        }
    }

    private Cloudiness mapCloudiness(final int cloudinessId) {
        if (cloudinessId == 511 || cloudinessId == 615 || cloudinessId == 616) {
            return SLEET;
        } else if (cloudinessId >= 200 && cloudinessId < 600) {
            return RAIN;
        } else if (cloudinessId >= 600 && cloudinessId < 700) {
            return SNOW;
        } else if (cloudinessId == 800) {
            return CLEAR;
        } else if (cloudinessId == 801 || cloudinessId == 802) {
            return CLOUDY;
        } else if (cloudinessId >= 803) {
            return OVERCAST;
        } else {
            return NONE;
        }
    }
}
