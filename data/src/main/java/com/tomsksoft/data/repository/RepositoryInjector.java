package com.tomsksoft.data.repository;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tomsksoft.data.cache.PreferencesWrapper;
import com.tomsksoft.data.webapi.IOpenWeatherService;
import com.tomsksoft.data.webapi.IYahooWeatherService;
import com.tomsksoft.data.webapi.IYandexWeatherService;
import com.tomsksoft.data.webapi.TokenInterceptor;
import com.tomsksoft.domain.base.repository.ICoordinatesRepository;
import com.tomsksoft.domain.base.repository.IOpenWeatherRepository;
import com.tomsksoft.domain.base.repository.IRatingRepository;
import com.tomsksoft.domain.base.repository.IRepositoryInjector;
import com.tomsksoft.domain.base.repository.IYahooWeatherRepository;
import com.tomsksoft.domain.base.repository.IYandexWeatherRepository;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepositoryInjector implements IRepositoryInjector {

    private final IOpenWeatherRepository mOpenWeatherRepository;
    private final IYandexWeatherRepository mYandexWeatherRepository;
    private final IYahooWeatherRepository mIYahooWeatherRepository;
    private final IRatingRepository mRatingRepository;
    private final ICoordinatesRepository mCoordinatesRepository;

    public RepositoryInjector(final Context context) {
        final Gson gson = new GsonBuilder().create();
        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://api.openweathermap.org")
                .build();

        final PreferencesWrapper preferencesWrapper = new PreferencesWrapper(context, gson);

        final IOpenWeatherService openWeatherService = retrofit.create(IOpenWeatherService.class);
        mOpenWeatherRepository = new OpenWeatherRepository(openWeatherService);

        final IYandexWeatherService yandexWeatherService = retrofit.create(IYandexWeatherService.class);
        mYandexWeatherRepository = new YandexWeatherRepository(yandexWeatherService);

        final IYahooWeatherService yahooWeatherService =
                new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(new OkHttpClient.Builder().addInterceptor(new TokenInterceptor()).build())
                        .baseUrl("https://weather-ydn-yql.media.yahoo.com")
                        .build()
                        .create(IYahooWeatherService.class);
        mIYahooWeatherRepository = new YahooWeatherRepository(yahooWeatherService);

        mRatingRepository = new RatingRepository(preferencesWrapper);
        mCoordinatesRepository = new CoordinatesRepository(preferencesWrapper);
    }

    @Override
    public IOpenWeatherRepository getOpenWeatherRepository() {
        return mOpenWeatherRepository;
    }

    @Override
    public IYandexWeatherRepository getYandexWeatherRepository() {
        return mYandexWeatherRepository;
    }

    @Override
    public IYahooWeatherRepository getYahooWeatherRepository() {
        return mIYahooWeatherRepository;
    }

    @Override
    public IRatingRepository getRatingRepository() {
        return mRatingRepository;
    }

    @Override
    public ICoordinatesRepository getCoordinatesRepository() {
        return mCoordinatesRepository;
    }
}
