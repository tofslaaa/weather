package com.tomsksoft.data.repository;

import com.tomsksoft.data.cache.PreferencesWrapper;
import com.tomsksoft.domain.base.repository.IRatingRepository;
import com.tomsksoft.domain.model.ProvidersRating;

import java.util.HashMap;

public class RatingRepository implements IRatingRepository {

    private final PreferencesWrapper mPreferencesWrapper;

    public RatingRepository(final PreferencesWrapper preferencesWrapper) {
        mPreferencesWrapper = preferencesWrapper;
    }

    @Override
    public ProvidersRating getProvidersRating() {
        return mPreferencesWrapper.getProvidersRating(new ProvidersRating(new HashMap<>()));
    }

    @Override
    public void saveProvidersRating(final ProvidersRating providersRating) {
        mPreferencesWrapper.putProvidersRating(providersRating);
    }
}
