package com.tomsksoft.data.repository;

import com.tomsksoft.data.cache.PreferencesWrapper;
import com.tomsksoft.domain.base.repository.ICoordinatesRepository;
import com.tomsksoft.domain.model.Coordinates;

public class CoordinatesRepository implements ICoordinatesRepository {

    private final PreferencesWrapper mPreferencesWrapper;

    public CoordinatesRepository(final PreferencesWrapper preferencesWrapper) {
        mPreferencesWrapper = preferencesWrapper;
    }

    @Override
    public Coordinates getLastCoordinates() {
        return mPreferencesWrapper.getLastCoordinates();
    }

    @Override
    public void saveLastCoordinates(final double longitude, final double latitude) {
        mPreferencesWrapper.putCoordinates(new Coordinates(longitude, latitude));
    }
}
