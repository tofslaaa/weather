package com.tomsksoft.data.repository;

import com.tomsksoft.data.model.YahooWeatherModel;
import com.tomsksoft.data.webapi.IYahooWeatherService;
import com.tomsksoft.domain.base.repository.IYahooWeatherRepository;
import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.Weather;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

import static com.tomsksoft.domain.model.Cloudiness.CLEAR;
import static com.tomsksoft.domain.model.Cloudiness.CLOUDY;
import static com.tomsksoft.domain.model.Cloudiness.NONE;
import static com.tomsksoft.domain.model.Cloudiness.OVERCAST;
import static com.tomsksoft.domain.model.Cloudiness.RAIN;
import static com.tomsksoft.domain.model.Cloudiness.SLEET;
import static com.tomsksoft.domain.model.Cloudiness.SNOW;

public class YahooWeatherRepository implements IYahooWeatherRepository {

    private final IYahooWeatherService mService;

    public YahooWeatherRepository(final IYahooWeatherService service) {
        mService = service;
    }

    @Override
    public Weather getCurrentWeather(final double lon, final double lat) throws Throwable {
        final YahooWeatherModel body = sendRequest(mService.getCurrentByCoord(lon, lat, "json", "c"));
        final YahooWeatherModel.CurrentObservation currentObservation = body.getCurrentObservation();
        final YahooWeatherModel.Wind wind = currentObservation.getWind();
        final YahooWeatherModel.Atmosphere atmosphere = currentObservation.getAtmosphere();
        final YahooWeatherModel.Condition condition = currentObservation.getCondition();
        return new Weather(
                condition.getTemperature(),
                body.getLocation().getCity(),
                wind.getSpeed(),
                wind.getDirection(),
                atmosphere.getPressure(),
                atmosphere.getHumidity(),
                mapCloudiness(condition.getCode())
        );
    }

    private <T> T sendRequest(final Call<T> call) throws IOException {
        final Response<T> response = call.execute();
        if (response.isSuccessful()) {
            final T body = response.body();
            if (body != null) {
                return body;
            } else {
                throw new RuntimeException("response body null");
            }
        } else {
            throw new RuntimeException(response.errorBody().string());
        }
    }

    private Cloudiness mapCloudiness(final int cloudiness) {
        if (cloudiness >= 0 && cloudiness <= 4 || cloudiness >= 9 && cloudiness <= 10 || cloudiness == 12 || cloudiness == 17 ||
            cloudiness == 35 || cloudiness >= 37 && cloudiness <= 40) {
            return RAIN;
        } else if (cloudiness >= 5 && cloudiness <= 8 || cloudiness == 18) {
            return SLEET;
        } else if (cloudiness == 11 || cloudiness >= 13 && cloudiness <= 16 || cloudiness >= 41 && cloudiness <= 43 ||
                   cloudiness >= 45 && cloudiness <= 47) {
            return SNOW;
        } else if (cloudiness >= 26 && cloudiness <= 28) {
            return OVERCAST;
        } else if (cloudiness >= 29 && cloudiness <= 30) {
            return CLOUDY;
        } else if (cloudiness >= 31 && cloudiness <= 34) {
            return CLEAR;
        } else {
            return NONE;
        }
    }
}
