package com.tomsksoft.data.cache;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tomsksoft.data.BuildConfig;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.ProvidersRating;

public class PreferencesWrapper {

    private final Gson mGson;
    private final static String PREFERENCES_NAME = BuildConfig.APPLICATION_ID;
    private final static String KEY_PROVIDERS_RATING = "KEY_PROVIDERS_RATING";
    private final static String KEY_LAST_COORDINATES = "KEY_LAST_COORDINATES";
    private final SharedPreferences mSharedPreferences;

    public PreferencesWrapper(final Context context, final Gson gson) {
        mGson = gson;
        mSharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void putProvidersRating(final ProvidersRating providersRating) {
        putValue(KEY_PROVIDERS_RATING, providersRating);
    }

    public ProvidersRating getProvidersRating(final ProvidersRating defaultValue) {
        return getValue(defaultValue, KEY_PROVIDERS_RATING, ProvidersRating.class);
    }

    public void putCoordinates(final Coordinates coordinates) {
        putValue(KEY_LAST_COORDINATES, coordinates);
    }

    public Coordinates getLastCoordinates() {
        return getValue(null, KEY_LAST_COORDINATES, Coordinates.class);
    }

    private <T> void putValue(final String key, final T value) {
        final String json = mGson.toJson(value);
        mSharedPreferences.edit()
                          .putString(key, json)
                          .apply();
    }

    private <T> T getValue(final T defaultValue, final String key, final Class<T> classOf) {
        final String json = mSharedPreferences.getString(key, "");
        if (json.isEmpty()) {
            return defaultValue;
        }
        try {
            return mGson.fromJson(json, classOf);
        } catch (final Throwable throwable) {
            return defaultValue;
        }
    }
}
