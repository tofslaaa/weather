package com.tomsksoft.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class YandexWeatherModelShort {

    @SerializedName("now")
    @Expose
    private long now;
    @SerializedName("now_dt")
    @Expose
    private String nowDt;
    @SerializedName("info")
    @Expose
    private Info info;
    @SerializedName("fact")
    @Expose
    private Fact fact;
    @SerializedName("forecast")
    @Expose
    private Forecast forecast;


    public class Fact {

        @SerializedName("temp")
        @Expose
        double temp;
        @SerializedName("feels_like")
        @Expose
        double feelsLike;
        @SerializedName("icon")
        @Expose
        String icon;
        @SerializedName("condition")
        @Expose
        String condition;
        @SerializedName("wind_speed")
        @Expose
        double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        double windGust;
        @SerializedName("wind_dir")
        @Expose
        String windDir;
        @SerializedName("pressure_mm")
        @Expose
        double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        double pressurePa;
        @SerializedName("humidity")
        @Expose
        double humidity;
        @SerializedName("daytime")
        @Expose
        String daytime;
        @SerializedName("polar")
        @Expose
        boolean polar;
        @SerializedName("season")
        @Expose
        String season;
        @SerializedName("obs_time")
        @Expose
        long obsTime;

        public double getTemp() {
            return temp;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public String getSeason() {
            return season;
        }

        public long getObsTime() {
            return obsTime;
        }
    }

    public class Forecast {

        @SerializedName("date")
        @Expose
        String date;
        @SerializedName("date_ts")
        @Expose
        long dateTs;
        @SerializedName("week")
        @Expose
        int week;
        @SerializedName("sunrise")
        @Expose
        String sunrise;
        @SerializedName("sunset")
        @Expose
        String sunset;
        @SerializedName("moon_code")
        @Expose
        int moonCode;
        @SerializedName("moon_text")
        @Expose
        String moonText;
        @SerializedName("parts")
        @Expose
        List<Part> parts = new ArrayList<Part>();

        public String getDate() {
            return date;
        }

        public long getDateTs() {
            return dateTs;
        }

        public int getWeek() {
            return week;
        }

        public String getSunrise() {
            return sunrise;
        }

        public String getSunset() {
            return sunset;
        }

        public int getMoonCode() {
            return moonCode;
        }

        public String getMoonText() {
            return moonText;
        }

        public List<Part> getParts() {
            return parts;
        }
    }

    public class Info {

        @SerializedName("lat")
        @Expose
        public double lat;
        @SerializedName("lon")
        @Expose
        public double lon;
        @SerializedName("url")
        @Expose
        String url;

        public double getLat() {
            return lat;
        }

        public double getLon() {
            return lon;
        }

        public String getUrl() {
            return url;
        }
    }

    public class Part {

        @SerializedName("part_name")
        @Expose
        String partName;
        @SerializedName("temp_min")
        @Expose
        double tempMin;
        @SerializedName("temp_max")
        @Expose
        double tempMax;
        @SerializedName("temp_avg")
        @Expose
        double tempAvg;
        @SerializedName("feels_like")
        @Expose
        double feelsLike;
        @SerializedName("icon")
        @Expose
        String icon;
        @SerializedName("condition")
        @Expose
        String condition;
        @SerializedName("daytime")
        @Expose
        String daytime;
        @SerializedName("polar")
        @Expose
        boolean polar;
        @SerializedName("wind_speed")
        @Expose
        double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        double windGust;
        @SerializedName("wind_dir")
        @Expose
        String windDir;
        @SerializedName("pressure_mm")
        @Expose
        double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        double pressurePa;
        @SerializedName("humidity")
        @Expose
        double humidity;
        @SerializedName("prec_mm")
        @Expose
        double precMm;
        @SerializedName("prec_period")
        @Expose
        int precPeriod;
        @SerializedName("prec_prob")
        @Expose
        int precProb;

        public String getPartName() {
            return partName;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        public double getTempAvg() {
            return tempAvg;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getPrecMm() {
            return precMm;
        }

        public int getPrecPeriod() {
            return precPeriod;
        }

        public int getPrecProb() {
            return precProb;
        }
    }

    public long getNow() {
        return now;
    }

    public String getNowDt() {
        return nowDt;
    }

    public Info getInfo() {
        return info;
    }

    public Fact getFact() {
        return fact;
    }

    public Forecast getForecast() {
        return forecast;
    }
}
