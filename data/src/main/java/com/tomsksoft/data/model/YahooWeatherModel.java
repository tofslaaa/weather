package com.tomsksoft.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class YahooWeatherModel {

    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("current_observation")
    @Expose
    private CurrentObservation currentObservation;
    @SerializedName("forecasts")
    @Expose
    private List<Forecast> forecasts = new ArrayList<>();

    public Location getLocation() {
        return location;
    }

    public CurrentObservation getCurrentObservation() {
        return currentObservation;
    }

    public List<Forecast> getForecasts() {
        return forecasts;
    }


    public class Wind {

        @SerializedName("chill")
        @Expose
        private int chill;
        @SerializedName("direction")
        @Expose
        private double direction;
        @SerializedName("speed")
        @Expose
        private double speed;

        public int getChill() {
            return chill;
        }

        public double getDirection() {
            return direction;
        }

        public double getSpeed() {
            return speed;
        }
    }

    public class Location {

        @SerializedName("woeid")
        @Expose
        private int woeid;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("region")
        @Expose
        private String region;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("lat")
        @Expose
        private double lat;
        @SerializedName("long")
        @Expose
        private double _long;
        @SerializedName("timezone_id")
        @Expose
        private String timezoneId;

        public int getWoeid() {
            return woeid;
        }

        public String getCity() {
            return city;
        }

        public String getRegion() {
            return region;
        }

        public String getCountry() {
            return country;
        }

        public double getLat() {
            return lat;
        }

        public double getLong() {
            return _long;
        }

        public String getTimezoneId() {
            return timezoneId;
        }
    }

    public class Forecast {

        @SerializedName("day")
        @Expose
        private String day;
        @SerializedName("date")
        @Expose
        private int date;
        @SerializedName("low")
        @Expose
        private int low;
        @SerializedName("high")
        @Expose
        private int high;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("code")
        @Expose
        private int code;

        public String getDay() {
            return day;
        }

        public int getDate() {
            return date;
        }

        public int getLow() {
            return low;
        }

        public int getHigh() {
            return high;
        }

        public String getText() {
            return text;
        }

        public int getCode() {
            return code;
        }
    }

    public class CurrentObservation {

        @SerializedName("wind")
        @Expose
        private Wind wind;
        @SerializedName("atmosphere")
        @Expose
        private Atmosphere atmosphere;
        @SerializedName("condition")
        @Expose
        private Condition condition;

        public Wind getWind() {
            return wind != null ? wind : new Wind();
        }

        public Atmosphere getAtmosphere() {
            return atmosphere != null ? atmosphere : new Atmosphere();
        }

        public Condition getCondition() {
            return condition != null ? condition : new Condition();
        }

    }

    public class Condition {

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("code")
        @Expose
        private int code;
        @SerializedName("temperature")
        @Expose
        private double temperature;

        public String getText() {
            return text;
        }

        public int getCode() {
            return code;
        }

        public double getTemperature() {
            return temperature;
        }
    }

    public class Atmosphere {

        @SerializedName("humidity")
        @Expose
        private double humidity;
        @SerializedName("visibility")
        @Expose
        private double visibility;
        @SerializedName("pressure")
        @Expose
        private double pressure;

        public double getHumidity() {
            return humidity;
        }

        public double getVisibility() {
            return visibility;
        }

        public double getPressure() {
            return pressure;
        }
    }

    public class Astronomy {

        @SerializedName("sunrise")
        @Expose
        private String sunrise;
        @SerializedName("sunset")
        @Expose
        private String sunset;

        public String getSunrise() {
            return sunrise;
        }

        public String getSunset() {
            return sunset;
        }
    }
}
