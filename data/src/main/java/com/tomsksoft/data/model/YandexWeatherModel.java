
package com.tomsksoft.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YandexWeatherModel {

    @SerializedName("now")
    @Expose
    public double now;
    @SerializedName("now_dt")
    @Expose
    public String nowDt;
    @SerializedName("info")
    @Expose
    public Info info;
    @SerializedName("fact")
    @Expose
    public Fact fact;
    @SerializedName("forecasts")
    @Expose
    public List<Forecast> forecasts = null;

    public double getNow() {
        return now;
    }

    public String getNowDt() {
        return nowDt;
    }

    public Info getInfo() {
        return info;
    }

    public Fact getFact() {
        return fact;
    }

    public List<Forecast> getForecasts() {
        return forecasts;
    }

    public static class Tzinfo {

        @SerializedName("offset")
        @Expose
        public double offset;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("abbr")
        @Expose
        public String abbr;
        @SerializedName("dst")
        @Expose
        public boolean dst;

        public double getOffset() {
            return offset;
        }

        public String getName() {
            return name;
        }

        public String getAbbr() {
            return abbr;
        }

        public boolean isDst() {
            return dst;
        }

    }

    public static class Parts {

        @SerializedName("night")
        @Expose
        public Night night;
        @SerializedName("morning")
        @Expose
        public Morning morning;
        @SerializedName("day")
        @Expose
        public Day day;
        @SerializedName("evening")
        @Expose
        public Evening evening;
        @SerializedName("day_short")
        @Expose
        public DayShort dayShort;
        @SerializedName("night_short")
        @Expose
        public NightShort nightShort;

        public Night getNight() {
            return night;
        }

        public Evening getEvening() {
            return evening;
        }

        public DayShort getDayShort() {
            return dayShort;
        }

        public NightShort getNightShort() {
            return nightShort;
        }

        public Day getDay() {
            return day;
        }

        public Morning getMorning() {
            return morning;
        }
    }

    public static class NightShort {
        @SerializedName("temp")
        @Expose
        public double temp;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;

        public double getTemp() {
            return temp;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecProb() {
            return precProb;
        }
    }

    public static class Night {

        @SerializedName("_source")
        @Expose
        public String source;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("temp_max")
        @Expose
        public double tempMax;
        @SerializedName("temp_avg")
        @Expose
        public double tempAvg;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("daytime")
        @Expose
        public String daytime;
        @SerializedName("polar")
        @Expose
        public boolean polar;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_period")
        @Expose
        public double precPeriod;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;

        public String getSource() {
            return source;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        public double getTempAvg() {
            return tempAvg;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecPeriod() {
            return precPeriod;
        }

        public double getPrecProb() {
            return precProb;
        }
    }

    public static class Info {


        @SerializedName("f")
        @Expose
        public boolean f;
        @SerializedName("n")
        @Expose
        public boolean n;
        @SerializedName("nr")
        @Expose
        public boolean nr;
        @SerializedName("ns")
        @Expose
        public boolean ns;
        @SerializedName("nsr")
        @Expose
        public boolean nsr;
        @SerializedName("p")
        @Expose
        public boolean p;
        @SerializedName("lat")
        @Expose
        public double lat;
        @SerializedName("lon")
        @Expose
        public double lon;
        @SerializedName("tzinfo")
        @Expose
        public Tzinfo tzinfo;
        @SerializedName("def_pressure_mm")
        @Expose
        public double defPressureMm;
        @SerializedName("def_pressure_pa")
        @Expose
        public double defPressurePa;
        @SerializedName("_h")
        @Expose
        public boolean h;
        @SerializedName("url")
        @Expose
        public String url;

        public boolean isF() {
            return f;
        }

        public boolean isN() {
            return n;
        }

        public boolean isNr() {
            return nr;
        }

        public boolean isNs() {
            return ns;
        }

        public boolean isNsr() {
            return nsr;
        }

        public boolean isP() {
            return p;
        }

        public double getLat() {
            return lat;
        }

        public double getLon() {
            return lon;
        }

        public Tzinfo getTzinfo() {
            return tzinfo;
        }

        public double getDefPressureMm() {
            return defPressureMm;
        }

        public double getDefPressurePa() {
            return defPressurePa;
        }

        public boolean isH() {
            return h;
        }

        public String getUrl() {
            return url;
        }
    }

    public static class Hour {

        @SerializedName("hour")
        @Expose
        public String hour;
        @SerializedName("hour_ts")
        @Expose
        public long hourTs;
        @SerializedName("temp")
        @Expose
        public double temp;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_period")
        @Expose
        public double precPeriod;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;
        @SerializedName("_nowcast")
        @Expose
        public boolean nowcast;

        public String getHour() {
            return hour;
        }

        public long getHourTs() {
            return hourTs;
        }

        public double getTemp() {
            return temp;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecPeriod() {
            return precPeriod;
        }

        public double getPrecProb() {
            return precProb;
        }

        public boolean isNowcast() {
            return nowcast;
        }
    }

    public static class Forecast {
        @SerializedName("date")
        @Expose
        public String date;
        @SerializedName("date_ts")
        @Expose
        public double dateTs;
        @SerializedName("week")
        @Expose
        public double week;
        @SerializedName("sunrise")
        @Expose
        public String sunrise;
        @SerializedName("sunset")
        @Expose
        public String sunset;
        @SerializedName("rise_begin")
        @Expose
        public String riseBegin;
        @SerializedName("set_end")
        @Expose
        public String setEnd;
        @SerializedName("moon_code")
        @Expose
        public double moonCode;
        @SerializedName("moon_text")
        @Expose
        public String moonText;
        @SerializedName("parts")
        @Expose
        public Parts parts;
        @SerializedName("hours")
        @Expose
        public List<Hour> hours = null;

        public String getDate() {
            return date;
        }

        public double getDateTs() {
            return dateTs;
        }

        public double getWeek() {
            return week;
        }

        public String getSunrise() {
            return sunrise;
        }

        public String getSunset() {
            return sunset;
        }

        public String getRiseBegin() {
            return riseBegin;
        }

        public String getSetEnd() {
            return setEnd;
        }

        public double getMoonCode() {
            return moonCode;
        }

        public String getMoonText() {
            return moonText;
        }

        public Parts getParts() {
            return parts;
        }

        public List<Hour> getHours() {
            return hours;
        }
    }

    public static class Fact {

        @SerializedName("temp")
        @Expose
        public double temp;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String cloudinessIcon;
        @SerializedName("condition")
        @Expose
        public String cloudiness;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDirection;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("daytime")
        @Expose
        public String daytime;
        @SerializedName("polar")
        @Expose
        public boolean polar;
        @SerializedName("season")
        @Expose
        public String season;
        @SerializedName("obs_time")
        @Expose
        public double obsTime;
        @SerializedName("source")
        @Expose
        public String source;

        public double getTemp() {
            return temp;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getCloudinessIcon() {
            return cloudinessIcon;
        }

        public String getCloudiness() {
            return cloudiness;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDirection() {
            return windDirection;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public String getSeason() {
            return season;
        }

        public double getObsTime() {
            return obsTime;
        }

        public String getSource() {
            return source;
        }
    }

    public static class Evening {

        @SerializedName("_source")
        @Expose
        public String source;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("temp_max")
        @Expose
        public double tempMax;
        @SerializedName("temp_avg")
        @Expose
        public double tempAvg;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("daytime")
        @Expose
        public String daytime;
        @SerializedName("polar")
        @Expose
        public boolean polar;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_period")
        @Expose
        public double precPeriod;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;

        public String getSource() {
            return source;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        public double getTempAvg() {
            return tempAvg;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecPeriod() {
            return precPeriod;
        }

        public double getPrecProb() {
            return precProb;
        }
    }

    public static class DayShort {

        @SerializedName("_source")
        @Expose
        public String source;
        @SerializedName("temp")
        @Expose
        public double temp;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;

        public String getSource() {
            return source;
        }

        public double getTemp() {
            return temp;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecProb() {
            return precProb;
        }
    }

    public static class Morning {

        @SerializedName("_source")
        @Expose
        public String source;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("temp_max")
        @Expose
        public double tempMax;
        @SerializedName("temp_avg")
        @Expose
        public double tempAvg;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("daytime")
        @Expose
        public String daytime;
        @SerializedName("polar")
        @Expose
        public boolean polar;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_period")
        @Expose
        public double precPeriod;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;

        public String getSource() {
            return source;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        public double getTempAvg() {
            return tempAvg;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecPeriod() {
            return precPeriod;
        }

        public double getPrecProb() {
            return precProb;
        }
    }

    public static class Day {

        @SerializedName("_source")
        @Expose
        public String source;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("temp_max")
        @Expose
        public double tempMax;
        @SerializedName("temp_avg")
        @Expose
        public double tempAvg;
        @SerializedName("feels_like")
        @Expose
        public double feelsLike;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("condition")
        @Expose
        public String condition;
        @SerializedName("daytime")
        @Expose
        public String daytime;
        @SerializedName("polar")
        @Expose
        public boolean polar;
        @SerializedName("wind_speed")
        @Expose
        public double windSpeed;
        @SerializedName("wind_gust")
        @Expose
        public double windGust;
        @SerializedName("wind_dir")
        @Expose
        public String windDir;
        @SerializedName("pressure_mm")
        @Expose
        public double pressureMm;
        @SerializedName("pressure_pa")
        @Expose
        public double pressurePa;
        @SerializedName("humidity")
        @Expose
        public double humidity;
        @SerializedName("uv_index")
        @Expose
        public double uvIndex;
        @SerializedName("soil_temp")
        @Expose
        public double soilTemp;
        @SerializedName("soil_moisture")
        @Expose
        public double soilMoisture;
        @SerializedName("prec_mm")
        @Expose
        public double precMm;
        @SerializedName("prec_period")
        @Expose
        public double precPeriod;
        @SerializedName("prec_prob")
        @Expose
        public double precProb;

        public String getSource() {
            return source;
        }

        public double getTempMin() {
            return tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        public double getTempAvg() {
            return tempAvg;
        }

        public double getFeelsLike() {
            return feelsLike;
        }

        public String getIcon() {
            return icon;
        }

        public String getCondition() {
            return condition;
        }

        public String getDaytime() {
            return daytime;
        }

        public boolean isPolar() {
            return polar;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindGust() {
            return windGust;
        }

        public String getWindDir() {
            return windDir;
        }

        public double getPressureMm() {
            return pressureMm;
        }

        public double getPressurePa() {
            return pressurePa;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getUvIndex() {
            return uvIndex;
        }

        public double getSoilTemp() {
            return soilTemp;
        }

        public double getSoilMoisture() {
            return soilMoisture;
        }

        public double getPrecMm() {
            return precMm;
        }

        public double getPrecPeriod() {
            return precPeriod;
        }

        public double getPrecProb() {
            return precProb;
        }
    }
}
