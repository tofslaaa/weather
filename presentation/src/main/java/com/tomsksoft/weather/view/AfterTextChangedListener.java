package com.tomsksoft.weather.view;

import android.text.Editable;
import android.text.TextWatcher;

public interface AfterTextChangedListener extends TextWatcher {

    @Override
    default void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) { }

    @Override
    default void onTextChanged(final CharSequence s, final int start, final int before, final int count) { }

    @Override
    void afterTextChanged(final Editable text);
}
