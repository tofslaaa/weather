package com.tomsksoft.weather.di;

import android.content.Context;

import com.tomsksoft.data.JobExecutor;
import com.tomsksoft.domain.base.repository.IRepositoryInjector;
import com.tomsksoft.domain.interactor.CalculateProvidersRatingInteractor;
import com.tomsksoft.domain.interactor.RequestAverageForecastInteractor;
import com.tomsksoft.domain.interactor.RequestAverageWeatherInteractor;
import com.tomsksoft.domain.interactor.RequestOpenWeatherCurrentInteractor;
import com.tomsksoft.domain.interactor.RequestOpenWeatherForecastInteractor;
import com.tomsksoft.domain.interactor.RequestProvidersRatingInteractor;
import com.tomsksoft.domain.interactor.RequestYahooCurrentInteractor;
import com.tomsksoft.domain.interactor.RequestYandexCurrentInteractor;
import com.tomsksoft.domain.interactor.RequestYandexForecastInteractor;
import com.tomsksoft.weather.converter.ConverterInjector;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Injector {

    private final RequestOpenWeatherCurrentInteractor mRequestOpenWeatherCurrentInteractor;
    private final RequestOpenWeatherForecastInteractor mRequestOpenWeatherForecastInteractor;
    private final RequestYandexCurrentInteractor mRequestYandexCurrentInteractor;
    private final RequestYandexForecastInteractor mRequestYandexForecastInteractor;
    private final RequestYahooCurrentInteractor mRequestYahooCurrentInteractor;
    private final RequestAverageWeatherInteractor mRequestAverageWeatherInteractor;
    private final RequestAverageForecastInteractor mRequestAverageForecastInteractor;
    private final ConverterInjector mConverterInjector;
    private final CalculateProvidersRatingInteractor mCalculateProvidersRatingInteractor;
    private final RequestProvidersRatingInteractor mRequestProvidersRatingInteractor;
    private final IRepositoryInjector mRepositoryInjector;

    public Injector(final Context context, final IRepositoryInjector repositoryInjector) {
        mRepositoryInjector = repositoryInjector;
        final ExecutorService executor = new JobExecutor(5, 10, 60L, TimeUnit.SECONDS);
        mRequestOpenWeatherCurrentInteractor = new RequestOpenWeatherCurrentInteractor(
                executor,
                repositoryInjector.getOpenWeatherRepository()
        );

        mRequestOpenWeatherForecastInteractor = new RequestOpenWeatherForecastInteractor(
                executor,
                repositoryInjector.getOpenWeatherRepository()
        );

        mRequestYandexCurrentInteractor = new RequestYandexCurrentInteractor(
                executor,
                repositoryInjector.getYandexWeatherRepository()
        );

        mRequestYandexForecastInteractor = new RequestYandexForecastInteractor(
                executor,
                repositoryInjector.getYandexWeatherRepository()
        );

        mRequestYahooCurrentInteractor = new RequestYahooCurrentInteractor(
                executor,
                repositoryInjector.getYahooWeatherRepository()
        );

        mRequestAverageWeatherInteractor = new RequestAverageWeatherInteractor(executor);
        mRequestAverageForecastInteractor = new RequestAverageForecastInteractor(executor);
        mCalculateProvidersRatingInteractor = new CalculateProvidersRatingInteractor(executor, repositoryInjector.getRatingRepository());
        mRequestProvidersRatingInteractor = new RequestProvidersRatingInteractor(executor, repositoryInjector.getRatingRepository());
        mConverterInjector = new ConverterInjector(context);
    }

    public RequestOpenWeatherCurrentInteractor getRequestOpenWeatherCurrentInteractor() {
        return mRequestOpenWeatherCurrentInteractor;
    }

    public RequestOpenWeatherForecastInteractor getRequestOpenWeatherForecastInteractor() {
        return mRequestOpenWeatherForecastInteractor;
    }

    public RequestYandexCurrentInteractor getRequestYandexCurrentInteractor() {
        return mRequestYandexCurrentInteractor;
    }

    public RequestYandexForecastInteractor getRequestYandexForecastInteractor() {
        return mRequestYandexForecastInteractor;
    }

    public RequestYahooCurrentInteractor getRequestYahooCurrentInteractor() {
        return mRequestYahooCurrentInteractor;
    }

    public RequestAverageWeatherInteractor getRequestAverageWeatherInteractor() {
        return mRequestAverageWeatherInteractor;
    }

    public RequestAverageForecastInteractor getRequestAverageForecastInteractor() {
        return mRequestAverageForecastInteractor;
    }

    public CalculateProvidersRatingInteractor getCalculateProvidersRatingInteractor() {
        return mCalculateProvidersRatingInteractor;
    }

    public RequestProvidersRatingInteractor getRequestProvidersRatingInteractor() {
        return mRequestProvidersRatingInteractor;
    }

    public ConverterInjector getConverterInjector() {
        return mConverterInjector;
    }

    public IRepositoryInjector getRepositoryInjector() {
        return mRepositoryInjector;
    }
}
