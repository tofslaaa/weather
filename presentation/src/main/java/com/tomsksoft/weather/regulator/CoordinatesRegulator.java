package com.tomsksoft.weather.regulator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Criteria;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

import com.tomsksoft.domain.base.repository.ICoordinatesRepository;
import com.tomsksoft.domain.model.Coordinates;

public class CoordinatesRegulator {

    private final LocationManager mLocationManager;
    private final ICoordinatesRepository mCoordinatesRepository;
    private final Context mContext;
    private final PermissionRegulator mPermissionRegulator;
    private final Looper mLooper;

    public CoordinatesRegulator(final Context context,
                                final PermissionRegulator permissionRegulator,
                                final ICoordinatesRepository coordinatesRepository) {
        mContext = context;
        mPermissionRegulator = permissionRegulator;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mCoordinatesRepository = coordinatesRepository;
        mLooper = Looper.getMainLooper();
    }

    public void requestCoordinatesUpdate(final CoordinatesListener listener) {
        final Coordinates last = mCoordinatesRepository.getLastCoordinates();
        if (last != null) {
            listener.onCoordinatesReceived(last.getLon(), last.getLat());
        }
        requestActualCoordinates(listener);
    }

    @SuppressLint("MissingPermission")
    public void requestActualCoordinates(final CoordinatesListener listener) {
        mPermissionRegulator.requestLocationPermission(() -> {
            final String provider = mLocationManager.getBestProvider(new Criteria(), true);
            final OnLocationChangedListener locationListener =
                    location -> {
                        mCoordinatesRepository.saveLastCoordinates(location.getLongitude(), location.getLatitude());
                        listener.onCoordinatesReceived(location.getLongitude(), location.getLatitude());
                    };

            mLocationManager.requestSingleUpdate(provider, locationListener, mLooper);
        });
    }


    public void requestLastCoordinates(final CoordinatesListener listener) {
        final Coordinates last = mCoordinatesRepository.getLastCoordinates();
        if (last != null) {
            listener.onCoordinatesReceived(last.getLon(), last.getLat());
        } else {
            requestActualCoordinates(listener);
        }
    }

    public Coordinates getLastCoordinates() {
        return mCoordinatesRepository.getLastCoordinates();
    }

    private interface OnLocationChangedListener extends LocationListener {

        @Override
        default void onStatusChanged(final String provider, final int status, final Bundle extras) {

        }

        @Override
        default void onProviderEnabled(final String provider) {

        }

        @Override
        default void onProviderDisabled(final String provider) {

        }
    }

    public interface CoordinatesListener {

        void onCoordinatesReceived(double longitude, double latitude);

        default void onFail() {}
    }
}
