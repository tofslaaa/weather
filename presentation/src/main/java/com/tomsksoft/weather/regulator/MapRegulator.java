package com.tomsksoft.weather.regulator;

import android.app.Activity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.tomsksoft.weather.R;

public class MapRegulator {

    private static final int ERROR_DIALOG_CODE = 9001;
    private final Activity mActivity;
    private final PermissionRegulator mPermissionRegulator;

    public MapRegulator(final Activity activity, final PermissionRegulator permissionRegulator) {
        mActivity = activity;
        mPermissionRegulator = permissionRegulator;
    }

    private boolean checkGoogleServices() {
        final GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        final int available = instance.isGooglePlayServicesAvailable(mActivity);
        if (available == ConnectionResult.SUCCESS) {
            return true;
        } else if (instance.isUserResolvableError(available)) {
            instance.getErrorDialog(mActivity, available, ERROR_DIALOG_CODE).show();
            return false;
        } else {
            Toast.makeText(mActivity, mActivity.getString(R.string.cant_make_map_request), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void prepareMap(final OnMapPreparedListener listener) {
        if (checkGoogleServices()) {
            mPermissionRegulator.requestLocationPermission(listener::action);
        }
    }

    public interface OnMapPreparedListener {

        void action();
    }
}
