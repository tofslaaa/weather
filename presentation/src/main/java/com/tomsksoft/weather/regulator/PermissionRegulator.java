package com.tomsksoft.weather.regulator;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import com.tomsksoft.weather.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class PermissionRegulator {

    private static final int PERMISSIONS_REQUEST_LOCATION = 99;
    private String mPermissionName = "";
    private PermissionsListener mPermissionsListener = new PermissionsListener.Null();

    private final Activity mContext;

    public PermissionRegulator(final Activity context) {
        mContext = context;
    }

    public void requestLocationPermission(final PermissionsListener permissionsListener) {
        mPermissionsListener = permissionsListener;
        mPermissionName = ACCESS_FINE_LOCATION;

        final boolean permissionDenied = ContextCompat.checkSelfPermission(mContext, mPermissionName) != PERMISSION_GRANTED;

        if (permissionDenied) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(mContext, mPermissionName)) {
                final DialogInterface.OnClickListener onClickListener =
                        (di, i) -> ActivityCompat.requestPermissions(
                                mContext,
                                new String[] {mPermissionName},
                                PERMISSIONS_REQUEST_LOCATION
                        );
                new AlertDialog.Builder(mContext)
                        .setTitle(R.string.title_location_permission)
                        .setPositiveButton(R.string.ok, onClickListener)
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(
                        mContext,
                        new String[] {mPermissionName},
                        PERMISSIONS_REQUEST_LOCATION
                );
            }
        } else {
            mPermissionsListener.onPermissionGranted();
            mPermissionsListener = new PermissionsListener.Null();
        }
    }

    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        final boolean permissionGranted = ContextCompat.checkSelfPermission(mContext, mPermissionName) == PERMISSION_GRANTED;
        if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                && grantResults[0] == PERMISSION_GRANTED
                && permissionGranted) {

                mPermissionsListener.onPermissionGranted();
                mPermissionsListener = new PermissionsListener.Null();
            } else {
                mPermissionsListener.onPermissionDenied();
                mPermissionsListener = new PermissionsListener.Null();
            }
        }
        mPermissionName = "";
    }

    public interface PermissionsListener {

        void onPermissionGranted();

        default void onPermissionDenied() {
        }

        class Null implements PermissionsListener {

            @Override
            public void onPermissionGranted() {

            }
        }
    }
}
