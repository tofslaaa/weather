package com.tomsksoft.weather.regulator;

import android.app.Activity;

import com.tomsksoft.weather.di.Injector;

public class RegulatorInjector {

    private final PermissionRegulator mPermissionRegulator;
    private final CoordinatesRegulator mCoordinatesRegulator;
    private final MapRegulator mMapRegulator;

    public RegulatorInjector(final Activity context, final Injector injector) {
        mPermissionRegulator = new PermissionRegulator(context);
        mCoordinatesRegulator =
                new CoordinatesRegulator(context, getPermissionRegulator(), injector.getRepositoryInjector().getCoordinatesRepository());
        mMapRegulator = new MapRegulator(context, mPermissionRegulator);
    }

    public CoordinatesRegulator getCoordinatesRegulator() {
        return mCoordinatesRegulator;
    }

    public PermissionRegulator getPermissionRegulator() {
        return mPermissionRegulator;
    }

    public MapRegulator getMapRegulator() {
        return mMapRegulator;
    }

    public void onRequestPermissionsResult(
            final int requestCode,
            final String[] permissions,
            final int[] grantResults) {
        mPermissionRegulator.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
