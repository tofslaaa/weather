package com.tomsksoft.weather.recycler.model;

import androidx.annotation.NonNull;

public class ProviderViewModel implements IViewModel {

    @NonNull
    private final Integer logoIcon;
    @NonNull
    private final Integer cloudinessIcon;
    @NonNull
    private final String temperature;


    public ProviderViewModel(@NonNull final Integer logoIcon,
                             @NonNull final Integer cloudinessIcon,
                             @NonNull final String temperature) {
        this.logoIcon = logoIcon;
        this.cloudinessIcon = cloudinessIcon;
        this.temperature = temperature;
    }

    @NonNull
    public Integer getLogoIcon() {
        return logoIcon;
    }

    @NonNull
    public Integer getCloudinessIcon() {
        return cloudinessIcon;
    }

    @NonNull
    public String getTemperature() {
        return temperature;
    }
}
