package com.tomsksoft.weather.recycler.model;

import androidx.annotation.NonNull;

public class WindViewModel implements IViewModel {

    @NonNull
    private final String wind;
    @NonNull
    private final String pressure;
    @NonNull
    private final String humidity;

    public WindViewModel(
            @NonNull final String wind,
            @NonNull final String pressure,
            @NonNull final String humidity) {
        this.wind = wind;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    @NonNull
    public String getWind() {
        return wind;
    }

    @NonNull
    public String getPressure() {
        return pressure;
    }

    @NonNull
    public String getHumidity() {
        return humidity;
    }
}
