package com.tomsksoft.weather.recycler.model;

import androidx.annotation.NonNull;

public class TemperatureViewModel implements IViewModel {

    @NonNull
    private final String time;
    @NonNull
    private final Integer cloudinessIcon;
    @NonNull
    private final String temperature;

    public TemperatureViewModel(
            @NonNull final String time,
            @NonNull final Integer cloudinessIcon,
            @NonNull final String temperature) {
        this.time = time;
        this.cloudinessIcon = cloudinessIcon;
        this.temperature = temperature;
    }

    @NonNull
    public String getTime() {
        return time;
    }

    @NonNull
    public Integer getCloudinessIcon() {
        return cloudinessIcon;
    }

    @NonNull
    public String getTemperature() {
        return temperature;
    }
}
