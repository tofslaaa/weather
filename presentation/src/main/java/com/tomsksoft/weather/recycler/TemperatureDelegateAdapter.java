package com.tomsksoft.weather.recycler;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseDelegateAdapter;
import com.tomsksoft.weather.base.BaseViewHolder;
import com.tomsksoft.weather.recycler.model.IViewModel;
import com.tomsksoft.weather.recycler.model.TemperatureViewModel;

public class TemperatureDelegateAdapter extends BaseDelegateAdapter<TemperatureDelegateAdapter.TemperatureViewHolder, TemperatureViewModel> {

    @Override
    protected void onBindViewHolder(
            @NonNull final View view,
            @NonNull final TemperatureViewModel item,
            @NonNull final TemperatureViewHolder viewHolder) {
        viewHolder.time.setText(item.getTime());
        viewHolder.icon.setImageResource(item.getCloudinessIcon());
        viewHolder.temperature.setText(item.getTemperature());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.temperature_type_item;
    }

    @NonNull
    @Override
    protected TemperatureViewHolder createViewHolder(final View parent) {
        return new TemperatureViewHolder(parent);
    }

    @Override
    public boolean isForViewType(@NonNull final IViewModel item) {
        return item instanceof TemperatureViewModel;
    }

    public class TemperatureViewHolder extends BaseViewHolder {

        private AppCompatImageView icon;
        private AppCompatTextView temperature;
        private AppCompatTextView time;

        public TemperatureViewHolder(@NonNull final View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time_average_forecast);
            icon = itemView.findViewById(R.id.icon_average_forecast);
            temperature = itemView.findViewById(R.id.temp_average_forecast);
        }
    }
}
