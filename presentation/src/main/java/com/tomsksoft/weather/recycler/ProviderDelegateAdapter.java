package com.tomsksoft.weather.recycler;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseDelegateAdapter;
import com.tomsksoft.weather.base.BaseViewHolder;
import com.tomsksoft.weather.recycler.model.IViewModel;
import com.tomsksoft.weather.recycler.model.ProviderViewModel;

public class ProviderDelegateAdapter
        extends BaseDelegateAdapter<ProviderDelegateAdapter.ProviderViewHolder, ProviderViewModel> {

    private Listener mListener;

    public ProviderDelegateAdapter(final Listener listener) {mListener = listener;}

    @Override
    protected void onBindViewHolder(@NonNull final View view,
                                    @NonNull final ProviderViewModel item,
                                    @NonNull final ProviderViewHolder viewHolder) {
        viewHolder.logoIcon.setImageResource(item.getLogoIcon());
        viewHolder.cloudinessIcon.setImageResource(item.getCloudinessIcon());
        viewHolder.temperature.setText(item.getTemperature());
        viewHolder.provider.setOnClickListener(v -> mListener.onModelClicked(item));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.provider_type_item;
    }

    @NonNull
    @Override
    protected ProviderViewHolder createViewHolder(final View parent) {
        return new ProviderViewHolder(parent);
    }

    @Override
    public boolean isForViewType(@NonNull final IViewModel item) {
        return item instanceof ProviderViewModel;
    }

    public class ProviderViewHolder extends BaseViewHolder {

        private AppCompatImageView logoIcon;
        private AppCompatImageView cloudinessIcon;
        private AppCompatTextView temperature;
        private View provider;

        public ProviderViewHolder(@NonNull final View itemView) {
            super(itemView);
            logoIcon = itemView.findViewById(R.id.providers_logo);
            cloudinessIcon = itemView.findViewById(R.id.cloudiness_provider_icon);
            temperature = itemView.findViewById(R.id.actual_provider_temperature);
            provider = itemView.findViewById(R.id.provider);
        }
    }

    public interface Listener {

        void onModelClicked(ProviderViewModel model);
    }
}
