package com.tomsksoft.weather.recycler;

import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tomsksoft.weather.recycler.model.IViewModel;

import java.util.ArrayList;
import java.util.List;

public class DelegatedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected final SparseArray<IDelegateAdapter> mDelegates;
    protected final List<IViewModel> mItems = new ArrayList<>();

    protected DelegatedAdapter(final SparseArray<IDelegateAdapter> delegates) {
        mDelegates = delegates;
    }

    @Override
    public int getItemViewType(final int position) {
        for (int i = 0; i < mDelegates.size(); i++) {
            final IDelegateAdapter delegate = mDelegates.valueAt(i);
            IViewModel item = mItems.get(position);
            if (delegate.isForViewType(item)) {
                return mDelegates.keyAt(i);
            }
        }
        throw new NullPointerException("Can not get viewType for position " + position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return mDelegates.get(viewType).onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final IDelegateAdapter delegateAdapter = mDelegates.get(getItemViewType(position));
        if (delegateAdapter != null) {
            //noinspection unchecked
            delegateAdapter.onBindViewHolder(holder, mItems, position);
        } else {
            throw new NullPointerException("can not find adapter for position " + position);
        }
    }

    @Override
    public void onViewRecycled(@NonNull final RecyclerView.ViewHolder holder) {
        mDelegates.get(holder.getItemViewType()).onRecycled(holder);
    }

    public void swapItems(@NonNull List<IViewModel> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class Builder {

        private int count;
        private final SparseArray<IDelegateAdapter> delegates;

        public Builder() {
            delegates = new SparseArray<>();
        }

        public Builder add(@NonNull IDelegateAdapter<?, ? extends IViewModel> delegateAdapter) {
            delegates.put(count++, delegateAdapter);
            return this;
        }

        public DelegatedAdapter build() {
            if (count == 0) {
                throw new IllegalArgumentException("Register at least one adapter");
            }
            return new DelegatedAdapter(delegates);
        }

    }

}
