package com.tomsksoft.weather.recycler;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseDelegateAdapter;
import com.tomsksoft.weather.base.BaseViewHolder;
import com.tomsksoft.weather.recycler.model.IViewModel;
import com.tomsksoft.weather.recycler.model.WindViewModel;

public class WindDelegateAdapter extends BaseDelegateAdapter<WindDelegateAdapter.WindViewHolder, WindViewModel> {

    @Override
    protected void onBindViewHolder(@NonNull final View view,
                                    @NonNull final WindViewModel item,
                                    @NonNull final WindViewHolder viewHolder) {
        viewHolder.wind.setText(item.getWind());
        viewHolder.pressure.setText(item.getPressure());
        viewHolder.humidity.setText(item.getHumidity());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.wind_type_item;
    }

    @NonNull
    @Override
    protected WindViewHolder createViewHolder(final View parent) {
        return new WindViewHolder(parent);
    }

    @Override
    public boolean isForViewType(@NonNull final IViewModel item) {
        return item instanceof WindViewModel;
    }

    public class WindViewHolder extends BaseViewHolder {

        private AppCompatTextView humidity;
        private AppCompatTextView pressure;
        private AppCompatTextView wind;

        public WindViewHolder(@NonNull final View itemView) {
            super(itemView);
            wind = itemView.findViewById(R.id.actual_wind);
            pressure = itemView.findViewById(R.id.actual_pressure);
            humidity = itemView.findViewById(R.id.actual_humidity);
        }

    }

}
