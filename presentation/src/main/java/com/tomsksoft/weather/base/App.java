package com.tomsksoft.weather.base;

import android.app.Application;

import com.tomsksoft.data.repository.RepositoryInjector;
import com.tomsksoft.weather.di.Injector;

public class App extends Application {

    private Injector mInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        mInjector = new Injector(this, new RepositoryInjector(this));
    }

    public Injector getInjector() {
        return mInjector;
    }

    public void setInjector(final Injector injector) {
        mInjector = injector;
    }
}
