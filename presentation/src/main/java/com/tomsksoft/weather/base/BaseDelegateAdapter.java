package com.tomsksoft.weather.base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tomsksoft.weather.recycler.IDelegateAdapter;

import java.util.List;

public abstract class BaseDelegateAdapter<VH extends BaseViewHolder, T> implements IDelegateAdapter<VH, T> {

    abstract protected void onBindViewHolder(@NonNull View view, @NonNull T item, @NonNull VH viewHolder);

    @LayoutRes
    abstract protected int getLayoutId();

    @NonNull
    abstract protected VH createViewHolder(View parent);

    @Override
    public void onRecycled(VH holder) { }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View inflatedView = LayoutInflater
                .from(parent.getContext())
                .inflate(getLayoutId(), parent, false);
        final VH holder = createViewHolder(inflatedView);
        holder.setListener((Object item, View view) -> onBindViewHolder(
                view,
                (T) item,
                holder
        ));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final VH holder, @NonNull final List<T> items, final int position) {
        holder.bind(items.get(position));
    }

}
