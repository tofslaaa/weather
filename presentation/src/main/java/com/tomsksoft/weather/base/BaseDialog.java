package com.tomsksoft.weather.base;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.tomsksoft.weather.converter.ConverterInjector;
import com.tomsksoft.weather.di.Injector;
import com.tomsksoft.weather.regulator.RegulatorInjector;

public abstract class BaseDialog extends DialogFragment {

    private final Handler mHandler = new Handler();

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreatePresenter();
    }

    protected void onCreatePresenter() { }

    protected Injector getInjector() {
        return ((App) getActivity().getApplication()).getInjector();
    }

    protected RegulatorInjector getRegulatorInjector() {
        return ((BaseActivity) getActivity()).getRegulatorInjector();
    }

    protected ConverterInjector getConverterInjector() { return getInjector().getConverterInjector();}

    protected void runOnMain(final Runnable runnable) {
        mHandler.post(runnable);
    }
}
