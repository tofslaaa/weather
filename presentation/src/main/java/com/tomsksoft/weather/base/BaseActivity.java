package com.tomsksoft.weather.base;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.tomsksoft.weather.regulator.RegulatorInjector;
import com.tomsksoft.weather.di.Injector;

public abstract class BaseActivity extends AppCompatActivity {

    private RegulatorInjector mRegulatorInjector;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRegulatorInjector = new RegulatorInjector(this, getInjector());
        onCreatePresenter();
    }

    protected void onCreatePresenter() { }

    public RegulatorInjector getRegulatorInjector() {
        return mRegulatorInjector;
    }

    protected Injector getInjector() {
        return getApp().getInjector();
    }

    private App getApp() {
        return (App) getApplication();
    }

    @Override
    public void onRequestPermissionsResult(
            final int requestCode,
            @NonNull final String[] permissions,
            @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getRegulatorInjector().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
