package com.tomsksoft.weather.base;

import android.util.Log;

import com.tomsksoft.domain.base.task.BaseTask;
import com.tomsksoft.domain.base.task.Listener;
import com.tomsksoft.domain.base.task.Task;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class BasePresenter {

    private final Set<Task> mTasks = new HashSet<>();
    public final String TAG = getClass().getSimpleName();

    public void start() {

    }

    protected <Result> void addTask(final BaseTask<Result> task, final Listener<Result> onResult) {
        addTask(task, onResult, throwable -> Log.e(TAG, "task onError: ", throwable));
    }

    protected <Result> void addTask(final BaseTask<Result> requestTask,
                                    final Listener<Result> onResult,
                                    final Listener<Throwable> onError) {
        mTasks.add(requestTask.listen(onResult, onError));
    }

    public void stop() {
        clear();
    }

    protected void clear() {
        final Iterator<Task> iterator = mTasks.iterator();
        while (iterator.hasNext()) {
            final Task next = iterator.next();
            next.cancel();
            iterator.remove();
        }
    }
}
