package com.tomsksoft.weather.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseFragment;
import com.tomsksoft.weather.regulator.CoordinatesRegulator.CoordinatesListener;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MapFragment extends BaseFragment {

    private static final float DEFAULT_ZOOM = 15f;
    private MapPresenter mPresenter;
    private CoordinatesListener mCoordinatesListener;
    private GoogleMap mMap;
    private Marker mMarker;
    private View mLocationSetButton;

    @Override
    protected void onCreatePresenter() {
        super.onCreatePresenter();
        mPresenter = new MapPresenter(mViewPresenter, getRegulatorInjector().getCoordinatesRegulator());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_map, container, false);
        mLocationSetButton = view.findViewById(R.id.location_set_view);
        mLocationSetButton.setOnClickListener(v -> mCoordinatesListener.onCoordinatesReceived(
                mMarker.getPosition().longitude,
                mMarker.getPosition().latitude
        ));
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMap -> {
            mMap = googleMap;
            mMap.setOnMapClickListener(latLng -> mPresenter.onMapClicked(latLng.longitude, latLng.latitude));
            //            mMap.setOnCameraMoveListener();
            mMap.setOnMyLocationChangeListener(location -> mPresenter.onMyLocationChanged(location.getLongitude(), location.getLatitude()));
            if (ActivityCompat.checkSelfPermission(requireContext(), ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(requireContext(), ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            initGoogleApi();
            mPresenter.onMapReady();
        });

        return view;
    }

    private void initGoogleApi() {
/*        mGoogleApiClient = new GoogleApiClient
                .Builder(requireContext())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(requireActivity(), connectionResult -> {

                })
                .build();*/
    }


    private void moveCamera(final LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MapFragment.DEFAULT_ZOOM));
        //        mMap.clear();
        //        mMarker.setPosition(latLng);
        //        final MarkerOptions options = new MarkerOptions().position(latLng);
        //        mMarker = mMap.addMarker(options);
        //        mMarker.setDraggable(true);
    }

    public void setCoordinatesListener(final CoordinatesListener coordinatesListener) {
        mCoordinatesListener = coordinatesListener;
    }

    private final MapPresenter.View mViewPresenter = new MapPresenter.View() {

        @Override
        public void moveMapCamera(final double longitude, final double latitude) {
            moveCamera(new LatLng(latitude, longitude));
        }

        @Override
        public void moveMarker(final double longitude, final double latitude) {
            if (mMarker != null) {
                mMarker.setPosition(new LatLng(latitude, longitude));
            } else {
                final MarkerOptions options = new MarkerOptions().position(new LatLng(latitude, longitude));
                mMarker = mMap.addMarker(options);
                mMarker.setDraggable(true);
            }
        }

    };

}
