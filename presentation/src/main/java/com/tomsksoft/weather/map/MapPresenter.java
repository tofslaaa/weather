package com.tomsksoft.weather.map;

import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.weather.base.BasePresenter;
import com.tomsksoft.weather.regulator.CoordinatesRegulator;

public class MapPresenter extends BasePresenter {

    private final View mView;
    private final CoordinatesRegulator mCoordinatesRegulator;
    private Coordinates mMyCoordinates;

    public MapPresenter(final View view, final CoordinatesRegulator coordinatesRegulator) {
        mView = view;
        mCoordinatesRegulator = coordinatesRegulator;
    }

    public void onMapReady() {
        mMyCoordinates = mCoordinatesRegulator.getLastCoordinates();
        if (mMyCoordinates != null) {
            mView.moveMapCamera(mMyCoordinates.getLon(), mMyCoordinates.getLat());
            mView.moveMarker(mMyCoordinates.getLon(), mMyCoordinates.getLat());
        }
    }

    public void onMapClicked(final double longitude, final double latitude) {
        mView.moveMarker(longitude, latitude);
    }

    public void onMyLocationChanged(final double longitude, final double latitude) {
        if (mMyCoordinates == null) {
            mView.moveMapCamera(longitude, latitude);
            mView.moveMarker(longitude, latitude);
        }
        mMyCoordinates = new Coordinates(longitude, latitude);
    }

    public interface View {

        void moveMapCamera(double longitude, double latitude);

        void moveMarker(double longitude, double latitude);

    }
}
