package com.tomsksoft.weather.home;

import android.text.TextUtils;
import android.util.Log;

import com.tomsksoft.domain.base.task.BaseTask;
import com.tomsksoft.domain.interactor.CalculateProvidersRatingInteractor;
import com.tomsksoft.domain.interactor.RequestAverageForecastInteractor;
import com.tomsksoft.domain.interactor.RequestAverageWeatherInteractor;
import com.tomsksoft.domain.interactor.RequestOpenWeatherCurrentInteractor;
import com.tomsksoft.domain.interactor.RequestOpenWeatherForecastInteractor;
import com.tomsksoft.domain.interactor.RequestProvidersRatingInteractor;
import com.tomsksoft.domain.interactor.RequestYahooCurrentInteractor;
import com.tomsksoft.domain.interactor.RequestYandexCurrentInteractor;
import com.tomsksoft.domain.interactor.RequestYandexForecastInteractor;
import com.tomsksoft.domain.model.AverageForecast;
import com.tomsksoft.domain.model.AverageWeather;
import com.tomsksoft.domain.model.Coordinates;
import com.tomsksoft.domain.model.Forecasts;
import com.tomsksoft.domain.model.Provider;
import com.tomsksoft.domain.model.UserReport;
import com.tomsksoft.domain.model.Weather;
import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BasePresenter;
import com.tomsksoft.weather.converter.BestProviderConverter;
import com.tomsksoft.weather.converter.CloudinessConverter;
import com.tomsksoft.weather.converter.CloudinessIconConverter;
import com.tomsksoft.weather.converter.HumidityConverter;
import com.tomsksoft.weather.converter.MillisConverter;
import com.tomsksoft.weather.converter.PressureConverter;
import com.tomsksoft.weather.converter.TemperatureConverter;
import com.tomsksoft.weather.converter.WindConverter;
import com.tomsksoft.weather.recycler.model.IViewModel;
import com.tomsksoft.weather.recycler.model.ProviderViewModel;
import com.tomsksoft.weather.recycler.model.TemperatureViewModel;
import com.tomsksoft.weather.recycler.model.WindViewModel;
import com.tomsksoft.weather.regulator.CoordinatesRegulator;
import com.tomsksoft.weather.regulator.MapRegulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomePresenter extends BasePresenter {

    private final CoordinatesRegulator mCoordinatesRegulator;
    private final MapRegulator mMapRegulator;

    private final RequestOpenWeatherCurrentInteractor mRequestOpenWeatherCurrent;
    private final RequestOpenWeatherForecastInteractor mRequestOpenWeatherForecast;
    private final RequestYandexCurrentInteractor mRequestYandexCurrent;
    private final RequestYandexForecastInteractor mRequestYandexForecast;
    private final RequestYahooCurrentInteractor mRequestYahooCurrent;
    private final RequestAverageWeatherInteractor mRequestAverage;
    private final RequestAverageForecastInteractor mRequestAverageForecast;
    private final CalculateProvidersRatingInteractor mCalculateProvidersRating;
    private final RequestProvidersRatingInteractor mRequestProvidersRating;

    private final TemperatureConverter mTemperatureConverter;
    private final CloudinessConverter mCloudinessConverter;
    private final CloudinessIconConverter mCloudinessIconConverter;
    private final WindConverter mWindConverter;
    private final PressureConverter mPressureConverter;
    private final HumidityConverter mHumidityConverter;
    private final MillisConverter mMillisConverter;
    private final BestProviderConverter mBestProviderConverter;
    private final View mView;

    private final static String TAG = HomePresenter.class.getSimpleName();

    private Coordinates mCoordinates;

    private Map<Provider, Weather> mCurrentWeatherMap;

    public HomePresenter(final CoordinatesRegulator coordinatesRegulator,
                         final MapRegulator mapRegulator,
                         final RequestOpenWeatherCurrentInteractor requestOpenWeatherCurrent,
                         final RequestOpenWeatherForecastInteractor requestOpenWeatherForecast,
                         final RequestYandexCurrentInteractor requestYandexCurrent,
                         final RequestYandexForecastInteractor requestYandexForecast,
                         final RequestYahooCurrentInteractor requestYahooCurrent,
                         final RequestAverageWeatherInteractor requestAverage,
                         final RequestAverageForecastInteractor requestAverageForecast,
                         final CalculateProvidersRatingInteractor calculateProvidersRating,
                         final RequestProvidersRatingInteractor requestProvidersRating,
                         final TemperatureConverter temperatureConverter,
                         final CloudinessConverter cloudinessConverter,
                         final CloudinessIconConverter cloudinessIconConverter,
                         final WindConverter windConverter,
                         final PressureConverter pressureConverter,
                         final HumidityConverter humidityConverter,
                         final MillisConverter millisConverter,
                         final BestProviderConverter bestProviderConverter,
                         final View view) {
        mCoordinatesRegulator = coordinatesRegulator;
        mMapRegulator = mapRegulator;
        mRequestOpenWeatherCurrent = requestOpenWeatherCurrent;
        mRequestOpenWeatherForecast = requestOpenWeatherForecast;
        mRequestYandexCurrent = requestYandexCurrent;
        mRequestYandexForecast = requestYandexForecast;
        mRequestYahooCurrent = requestYahooCurrent;
        mRequestAverage = requestAverage;
        mRequestAverageForecast = requestAverageForecast;
        mCalculateProvidersRating = calculateProvidersRating;
        mRequestProvidersRating = requestProvidersRating;
        mTemperatureConverter = temperatureConverter;
        mCloudinessConverter = cloudinessConverter;
        mCloudinessIconConverter = cloudinessIconConverter;
        mWindConverter = windConverter;
        mPressureConverter = pressureConverter;
        mHumidityConverter = humidityConverter;
        mMillisConverter = millisConverter;
        mBestProviderConverter = bestProviderConverter;
        mView = view;
    }

    @Override
    public void start() {
        super.start();
        mCurrentWeatherMap = new HashMap<>();

        mRequestProvidersRating.request(true)
                               .listen(result -> mView.updateBestProvider(mBestProviderConverter.convert(result.getBestProvider())));

        mCoordinatesRegulator.requestLastCoordinates(this::onCoordinatesChanged);

        mMapRegulator.prepareMap(mView::showMapPreview);
    }

    private List<IViewModel> mapForecastModels(final AverageWeather weather, final List<AverageForecast> forecasts) {
        Log.d(TAG, "mapForecastModels: " + weather + ", " + forecasts);
        final String wind = mWindConverter.convert(new WindConverter.Params(
                weather.getWindSpeed(),
                weather.getWindDirection()
        ));
        final String pressure = mPressureConverter.convert(weather.getPressure());
        final String humidity = mHumidityConverter.convert(weather.getHumidity());

        final List<IViewModel> models = new ArrayList<>(9);
        models.clear();
        models.add(new WindViewModel(wind, pressure, humidity));

        final List<IViewModel> temperatureModels = new ArrayList<>();
        for (final AverageForecast averageForecast : forecasts) {
            final String time = mMillisConverter.convert(averageForecast.getMillis());
            final Integer cloudinessIcon = mCloudinessIconConverter.convert(averageForecast.getCloudiness());
            final String temperature = mTemperatureConverter.convert(averageForecast.getTemperature());
            temperatureModels.add(new TemperatureViewModel(time, cloudinessIcon, temperature));
            if (temperatureModels.size() == 8) {
                break;
            }
        }

        models.addAll(temperatureModels);
        return models;
    }

    private List<IViewModel> mapProviders(final Weather openWeather, final Weather yandexWeather, final Weather yahooWeather) {
        final List<IViewModel> providersModels = new ArrayList<>();
        providersModels.add(new ProviderViewModel(
                R.drawable.ic_logo_yandexweather,
                mCloudinessIconConverter.convert(yandexWeather.getCloudiness()),
                mTemperatureConverter.convert(yandexWeather.getTemperature())
        ));
        providersModels.add(new ProviderViewModel(
                R.drawable.ic_logo_openweathermap,
                mCloudinessIconConverter.convert(openWeather.getCloudiness()),
                mTemperatureConverter.convert(openWeather.getTemperature())
        ));
        providersModels.add(new ProviderViewModel(
                R.drawable.ic_logo_yahoo,
                mCloudinessIconConverter.convert(yahooWeather.getCloudiness()),
                mTemperatureConverter.convert(yahooWeather.getTemperature())
        ));
        return providersModels;
    }


    private void updateMainWeather(final AverageWeather average) {
        mView.updateAverageTemperature(mTemperatureConverter.convert(average.getTemperature()));
        mView.updateAverageCloudiness(mCloudinessConverter.convert(average.getCloudiness()));
        mView.updateCloudinessIcon(mCloudinessIconConverter.convert(average.getCloudiness()));
    }

    private void updateMainWeather(final Weather weather) {
        mView.updateAverageTemperature(mTemperatureConverter.convert(weather.getTemperature()));
        mView.updateAverageCloudiness(mCloudinessConverter.convert(weather.getCloudiness()));
        mView.updateCloudinessIcon(mCloudinessIconConverter.convert(weather.getCloudiness()));
    }

    public void onUserReportReceived(final UserReport userReport) {
        mCalculateProvidersRating.request(new CalculateProvidersRatingInteractor.Params(userReport, mCurrentWeatherMap))
                                 .listen(result -> mView.updateBestProvider(mBestProviderConverter.convert(result.getBestProvider())));
    }

    public void onCoordinatesChanged(final double lon, final double lat) {
        clear();
        mCoordinates = new Coordinates(lon, lat);
        Log.d(TAG, "onCoordinatedReceived: " + mCoordinates);

        final BaseTask<Weather> openWeatherCurrentTask = mRequestOpenWeatherCurrent.request(mCoordinates);
        final BaseTask<Weather> yandexWeatherCurrentTask = mRequestYandexCurrent.request(mCoordinates);
        final BaseTask<Weather> yahooWeatherCurrentTask = mRequestYahooCurrent.request(mCoordinates);

        final BaseTask<Forecasts> openWeatherForecastTask = mRequestOpenWeatherForecast.request(mCoordinates);
        final BaseTask<Forecasts> yandexWeatherForecastTask = mRequestYandexForecast.request(mCoordinates);

        final BaseTask<List<AverageForecast>> averageForecastsTask = openWeatherForecastTask
                .with(yandexWeatherForecastTask, RequestAverageForecastInteractor.Params::new)
                .andThen(mRequestAverageForecast::request);

        final BaseTask<AverageWeather> averageWeatherTask = openWeatherCurrentTask
                .with(yandexWeatherCurrentTask, yahooWeatherCurrentTask, RequestAverageWeatherInteractor.Params::new)
                .andThen(mRequestAverage::request);

        addTask(openWeatherCurrentTask, openWeather -> {
            mCurrentWeatherMap.put(Provider.OPEN_WEATHER, openWeather);

            Log.d(TAG, "open weather received: " + openWeather);
            updateMainWeather(openWeather);
        });

        addTask(yandexWeatherCurrentTask, yandexWeather -> {
            mCurrentWeatherMap.put(Provider.YANDEX, yandexWeather);

            Log.d(TAG, "yandex weather received: " + yandexWeather);
            updateMainWeather(yandexWeather);
            final String location = yandexWeather.getLocation();
            if (!TextUtils.isEmpty(location)) {
                mView.updateCityName(location);
            }
        });

        addTask(yahooWeatherCurrentTask, yahooWeather -> {
            mCurrentWeatherMap.put(Provider.YAHOO, yahooWeather);

            Log.d(TAG, "yahoo weather received: " + yahooWeather);
            updateMainWeather(yahooWeather);
        });

        addTask(
                openWeatherCurrentTask.with(yandexWeatherCurrentTask, yahooWeatherCurrentTask, this::mapProviders),
                mView::updateProvidersItems
        );

        addTask(
                averageWeatherTask.with(averageForecastsTask, this::mapForecastModels),
                mView::updateForecastsItems
        );

        addTask(averageWeatherTask, average -> {
            Log.d(TAG, "average weather received: " + average);
            updateMainWeather(average);
        });

    }

    public interface View {

        void updateCityName(String cityName);

        void updateAverageTemperature(String temperature);

        void updateAverageCloudiness(String cloudiness);

        void updateCloudinessIcon(Integer cloudinessIcon);

        void updateForecastsItems(final List<IViewModel> items);

        void updateProvidersItems(final List<IViewModel> items);

        void updateBestProvider(String bestProvider);

        void showMapPreview();
    }

}
