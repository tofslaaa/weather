package com.tomsksoft.weather.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.UserReport;
import com.tomsksoft.domain.model.WindType;
import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseFragment;
import com.tomsksoft.weather.map.MapFragment;
import com.tomsksoft.weather.recycler.DelegatedAdapter;
import com.tomsksoft.weather.recycler.ProviderDelegateAdapter;
import com.tomsksoft.weather.recycler.TemperatureDelegateAdapter;
import com.tomsksoft.weather.recycler.WindDelegateAdapter;
import com.tomsksoft.weather.recycler.model.IViewModel;
import com.tomsksoft.weather.report.ReportDialog;

import java.util.List;

public class HomeFragment extends BaseFragment {

    private HomePresenter mPresenter;
    private TextView mActualTemperature;
    private TextView mCity;
    private TextView mActualCloudiness;
    private ImageView mActualCloudinessImage;
    private TextView mBestProvider;

    private DelegatedAdapter mForecastsAdapter;
    private DelegatedAdapter mProvidersAdapter;

    private static final int REQUEST_REPORT_USERS_DATA = 1;
    private View mOverlay;
    private ScrollView mScrollView;


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        mActualTemperature = view.findViewById(R.id.actual_temperature);
        mCity = view.findViewById(R.id.city);
        mActualCloudiness = view.findViewById(R.id.cloudiness_text);
        mActualCloudinessImage = view.findViewById(R.id.cloudiness_icon);
        mBestProvider = view.findViewById(R.id.most_accurate_service);

        final View reportWeather = view.findViewById(R.id.report_actual_weather_container);
        reportWeather.setOnClickListener(v -> {
            final DialogFragment fragment = new ReportDialog();
            fragment.setTargetFragment(HomeFragment.this, REQUEST_REPORT_USERS_DATA);
            fragment.show(HomeFragment.this.getFragmentManager(), fragment.getClass().getName());
        });

        /*
        final RequestBuilder<PictureDrawable> requestBuilder = Glide.with(this)
                                                                    .as(PictureDrawable.class)
                                                                    .transition(withCrossFade())
                                                                    .listener(new SvgSoftwareLayerSetter());
        */
        mForecastsAdapter = new DelegatedAdapter.Builder()
                .add(new WindDelegateAdapter())
                .add(new TemperatureDelegateAdapter())
                .build();

        final RecyclerView forecastRecyclerView = view.findViewById(R.id.hour_forecast_recycler);
        forecastRecyclerView.setLayoutManager(new LinearLayoutManager(
                requireContext(),
                RecyclerView.HORIZONTAL,
                false
        ));
        forecastRecyclerView.setAdapter(mForecastsAdapter);

        mProvidersAdapter = new DelegatedAdapter.Builder()
                .add(new ProviderDelegateAdapter(model -> {
                    //TODO ProviderFragment etc..
                }))
                .build();

        final RecyclerView providerRecyclerView = view.findViewById(R.id.providers_recycler);
        providerRecyclerView.setLayoutManager(new LinearLayoutManager(
                requireContext(),
                RecyclerView.VERTICAL,
                false
        ));
        providerRecyclerView.setAdapter(mProvidersAdapter);

        mOverlay = view.findViewById(R.id.transparent_image);
        mScrollView = view.findViewById(R.id.scroll_view);

        mOverlay.setOnTouchListener(mMapTouchListener);
        return view;
    }

    @Override
    protected void onCreatePresenter() {
        super.onCreatePresenter();
        mPresenter = new HomePresenter(
                getRegulatorInjector().getCoordinatesRegulator(),
                getRegulatorInjector().getMapRegulator(),
                getInjector().getRequestOpenWeatherCurrentInteractor(),
                getInjector().getRequestOpenWeatherForecastInteractor(),
                getInjector().getRequestYandexCurrentInteractor(),
                getInjector().getRequestYandexForecastInteractor(),
                getInjector().getRequestYahooCurrentInteractor(),
                getInjector().getRequestAverageWeatherInteractor(),
                getInjector().getRequestAverageForecastInteractor(),
                getInjector().getCalculateProvidersRatingInteractor(),
                getInjector().getRequestProvidersRatingInteractor(),
                getConverterInjector().getTemperatureConverter(),
                getConverterInjector().getCloudinessConverter(),
                getConverterInjector().getCloudinessIconConverter(),
                getConverterInjector().getWindConverter(),
                getConverterInjector().getPressureConverter(),
                getConverterInjector().getHumidityConverter(),
                getConverterInjector().getMillisConverter(),
                getConverterInjector().getBestProviderConverter(),
                mView
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.stop();
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_REPORT_USERS_DATA:
                    assert data != null;
                    final String stringCloudiness = data.getStringExtra(ReportDialog.TAG_REPORT_CLOUDINESS);
                    final double temperature = data.getDoubleExtra(ReportDialog.TAG_REPORT_TEMPERATURE, requestCode);
                    final String stringWind = data.getStringExtra(ReportDialog.TAG_REPORT_WIND);

                    mPresenter.onUserReportReceived(new UserReport(
                            Enum.valueOf(Cloudiness.class, stringCloudiness),
                            temperature,
                            Enum.valueOf(WindType.class, stringWind)
                    ));
                    break;
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener mMapTouchListener = (v, event) -> {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept touch events.
                mScrollView.requestDisallowInterceptTouchEvent(true);
                // Disable touch on transparent view
                return false;

            case MotionEvent.ACTION_UP:
                // Allow ScrollView to intercept touch events.
                mScrollView.requestDisallowInterceptTouchEvent(false);
                return true;
            case MotionEvent.ACTION_MOVE:
                mScrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            default:
                return true;
        }
    };

    private final HomePresenter.View mView = new HomePresenter.View() {

        @Override
        public void updateCityName(final String cityName) {
            runOnMain(() -> mCity.setText(cityName));
        }

        @Override
        public void updateAverageTemperature(final String temperature) {
            runOnMain(() -> mActualTemperature.setText(temperature));
        }

        @Override
        public void updateAverageCloudiness(final String cloudiness) {
            runOnMain(() -> mActualCloudiness.setText(cloudiness));
        }

        @Override
        public void updateCloudinessIcon(final Integer cloudinessIcon) {
            runOnMain(() -> mActualCloudinessImage.setImageResource(cloudinessIcon));
        }

        @Override
        public void updateForecastsItems(final List<IViewModel> items) {
            runOnMain(() -> mForecastsAdapter.swapItems(items));
        }

        @Override
        public void updateProvidersItems(final List<IViewModel> items) {
            runOnMain(() -> mProvidersAdapter.swapItems(items));
        }

        @Override
        public void updateBestProvider(final String bestProvider) {
            runOnMain(() -> mBestProvider.setText(bestProvider));
        }

        @Override
        public void showMapPreview() {
            final FragmentManager fragmentManager = getChildFragmentManager();
            final String tag = MapFragment.class.getName();
            final MapFragment fragmentByTag = (MapFragment) fragmentManager.findFragmentByTag(tag);
            if (fragmentByTag == null) {
                final MapFragment fragment = new MapFragment();
                fragmentManager.beginTransaction()
                               .add(R.id.map_container, fragment, tag)
                               .commitNowAllowingStateLoss();
                fragment.setCoordinatesListener((lon, lat) -> mPresenter.onCoordinatesChanged(lon, lat));
            } else {
                fragmentByTag.setCoordinatesListener((lon, lat) -> mPresenter.onCoordinatesChanged(lon, lat));
            }
        }
    };

}
