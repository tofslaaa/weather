package com.tomsksoft.weather.converter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MillisConverter implements Converter<Long, String> {

    @Override
    public String convert(final Long millis) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        simpleDateFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return simpleDateFormat.format(millis);
    }
}
