package com.tomsksoft.weather.converter;

public class PressureConverter implements Converter<Double, String> {

    @Override
    public String convert(final Double pressure) {
        return Math.round(pressure) + " mmHg";
    }
}
