package com.tomsksoft.weather.converter;

import com.tomsksoft.domain.model.Cloudiness;

public class CloudinessConverter implements Converter<Cloudiness, String> {

    @Override
    public String convert(final Cloudiness cloudiness) {
        switch (cloudiness) {
            case CLEAR:
                return "Clear";
            case RAIN:
                return "Rain";
            case SNOW:
                return "Snow";
            case SLEET:
                return "Sleet";
            case CLOUDY:
                return "Partly cloudy";
            case OVERCAST:
                return "Overcast";
            case NONE:
            default:
                return "";
        }
    }
}
