package com.tomsksoft.weather.converter;

public interface Converter<T, U> {
    U convert(T t);
}
