package com.tomsksoft.weather.converter;

import android.content.Context;
import android.content.res.Resources;

import com.tomsksoft.domain.model.Provider;
import com.tomsksoft.weather.R;

public class BestProviderConverter implements Converter<Provider,String> {

    private final Context mContext;

    public BestProviderConverter(final Context context) {
        mContext = context;
    }

    @Override
    public String convert(final Provider provider) {
        final Resources resources = mContext.getResources();
        final String bestProviderString = resources.getString(R.string.best_service);

        switch (provider) {
            case OPEN_WEATHER:
                return String.format(bestProviderString, "OpenWeatherMap");
            case YANDEX:
                return String.format(bestProviderString, "Яндекс.Погода");
            default:
                return resources.getString(R.string.service_unknown);
        }
    }
}
