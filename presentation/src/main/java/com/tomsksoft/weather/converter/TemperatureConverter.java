package com.tomsksoft.weather.converter;

public class TemperatureConverter implements Converter<Double, String> {

    @Override
    public String convert(final Double temperature) {
        final long rounded = Math.round(temperature);
        return (rounded > 0 ? "+" : "") + rounded + "°";
    }
}
