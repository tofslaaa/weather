package com.tomsksoft.weather.converter;

import android.content.Context;

import com.tomsksoft.weather.R;

public class WindConverter implements Converter<WindConverter.Params, String> {

    private final Context mContext;

    public WindConverter(final Context context) {
        mContext = context;
    }

    @Override
    public String convert(final Params params) {
        return mContext.getString(
                R.string.wind_speed_direction,
                Math.round(params.getWindSpeed()),
                mapWindDirection(params.getWindDirection())
        );
    }

    private String mapWindDirection(final double windDirection) {
        final double step = 45. / 2.;
        final int resId;
        if (windDirection >= step * 15 || windDirection <= step) {
            resId = R.string.direction_n;
        } else if (windDirection <= step * 3) {
            resId = R.string.direction_ne;
        } else if (windDirection <= step * 5) {
            resId = R.string.direction_e;
        } else if (windDirection <= step * 7) {
            resId = R.string.direction_se;
        } else if (windDirection <= step * 9) {
            resId = R.string.direction_s;
        } else if (windDirection <= step * 11) {
            resId = R.string.direction_sw;
        } else if (windDirection <= step * 13) {
            resId = R.string.direction_w;
        } else if (windDirection <= step * 15) {
            resId = R.string.direction_nw;
        } else {
            resId = -1;
        }
        return resId != -1 ? mContext.getString(resId) : "";
    }

    public static final class Params {

        private final double windSpeed;
        private final double windDirection;

        public Params(final double windSpeed, final double windDirection) {
            this.windSpeed = windSpeed;
            this.windDirection = windDirection;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public double getWindDirection() {
            return windDirection;
        }
    }
}
