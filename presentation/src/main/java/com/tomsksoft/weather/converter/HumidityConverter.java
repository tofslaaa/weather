package com.tomsksoft.weather.converter;

public class HumidityConverter implements Converter<Double, String> {

    @Override
    public String convert(final Double humidity) {
        return Math.round(humidity) + "%";
    }
}
