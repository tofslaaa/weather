package com.tomsksoft.weather.converter;

import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.weather.R;

public class CloudinessIconConverter implements Converter<Cloudiness, Integer> {

    @Override
    public Integer convert(final Cloudiness cloudiness) {
        switch (cloudiness) {
            case CLEAR:
                return R.drawable.ic_clear;
            case CLOUDY:
                return R.drawable.ic_cloudy;
            case OVERCAST:
                return R.drawable.ic_overcast;
            case RAIN:
                return R.drawable.ic_rain;
            case SLEET:
                return R.drawable.ic_sleet;
            case SNOW:
                return R.drawable.ic_snow;
            case NONE:
            default:
                return R.drawable.transparent;
        }
    }
}
