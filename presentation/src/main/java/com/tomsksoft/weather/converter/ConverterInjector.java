package com.tomsksoft.weather.converter;

import android.content.Context;

public class ConverterInjector {

    private final TemperatureConverter mTemperatureConverter;
    private final CloudinessConverter mCloudinessConverter;
    private final CloudinessIconConverter mCloudinessIconConverter;
    private final WindConverter mWindConverter;
    private final PressureConverter mPressureConverter;
    private final HumidityConverter mHumidityConverter;
    private final MillisConverter mMillisConverter;
    private final BestProviderConverter mBestProviderConverter;

    public ConverterInjector(Context context) {
        mTemperatureConverter = new TemperatureConverter();
        mCloudinessConverter = new CloudinessConverter();
        mCloudinessIconConverter = new CloudinessIconConverter();
        mWindConverter = new WindConverter(context);
        mPressureConverter = new PressureConverter();
        mHumidityConverter = new HumidityConverter();
        mMillisConverter = new MillisConverter();
        mBestProviderConverter = new BestProviderConverter(context);
    }

    public TemperatureConverter getTemperatureConverter() {
        return mTemperatureConverter;
    }

    public CloudinessConverter getCloudinessConverter() {
        return mCloudinessConverter;
    }

    public CloudinessIconConverter getCloudinessIconConverter() {
        return mCloudinessIconConverter;
    }

    public WindConverter getWindConverter() {
        return mWindConverter;
    }

    public PressureConverter getPressureConverter() {
        return mPressureConverter;
    }

    public HumidityConverter getHumidityConverter() {
        return mHumidityConverter;
    }

    public MillisConverter getMillisConverter() {
        return mMillisConverter;
    }

    public BestProviderConverter getBestProviderConverter() {
        return mBestProviderConverter;
    }
}
