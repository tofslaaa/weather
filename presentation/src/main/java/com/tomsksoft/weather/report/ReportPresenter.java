package com.tomsksoft.weather.report;

import android.text.Editable;

import com.tomsksoft.domain.model.Cloudiness;
import com.tomsksoft.domain.model.UserReport;
import com.tomsksoft.domain.model.WindType;
import com.tomsksoft.weather.base.BasePresenter;

public class ReportPresenter extends BasePresenter {

    private final State mState = new State();
    private final View mView;

    private final static String TAG = ReportPresenter.class.getSimpleName();

    public ReportPresenter(final View view) {
        mView = view;
    }

    @Override
    public void start() {
        super.start();
    }

    public void onClearCloudinessClicked() {
        onCloudinessChanged(Cloudiness.CLEAR);
    }

    public void onCloudyCloudinessClicked() {
        onCloudinessChanged(Cloudiness.CLOUDY);
    }

    public void onOvercastCloudinessClicked() {
        onCloudinessChanged(Cloudiness.OVERCAST);
    }

    public void onSleetCloudinessClicked() {
        onCloudinessChanged(Cloudiness.SLEET);
    }

    public void onRainCloudinessClicked() {
        onCloudinessChanged(Cloudiness.RAIN);
    }

    public void onSnowCloudinessClicked() {
        onCloudinessChanged(Cloudiness.SNOW);
    }

    private void onCloudinessChanged(final Cloudiness cloudiness) {
        mState.cloudiness = cloudiness;
        onStateChanged();
    }

    private void changeSelectedCloudiness(final Cloudiness cloudiness) {
        mView.unselectCloudiness();

        switch (cloudiness) {
            case CLEAR:
                mView.selectClearCloudiness();
                break;
            case CLOUDY:
                mView.selectCloudyCloudiness();
                break;
            case OVERCAST:
                mView.selectOvercastCloudiness();
                break;
            case SLEET:
                mView.selectSleetCloudiness();
                break;
            case RAIN:
                mView.selectRainCloudiness();
                break;
            case SNOW:
                mView.selectSnowCloudiness();
                break;
            case NONE:
            default:
                break;
        }
    }

    public void onTemperatureChanged(final Editable text) {
        try {
            mState.temperature = Double.valueOf(text.toString());
        } catch (final Throwable throwable) {
            mState.temperature = Double.NaN;
        }
        onStateChanged();
    }

    public void onCalmWindChecked(final boolean checked) {
        if (checked) {
            mState.wind = WindType.CALM;
            onStateChanged();
        }
    }

    public void onLightWindChecked(final boolean checked) {
        if (checked) {
            mState.wind = WindType.LIGHT;
            onStateChanged();
        }
    }

    public void onModerateWindChecked(final boolean checked) {
        if (checked) {
            mState.wind = WindType.MODERATE;
            onStateChanged();
        }
    }

    public void onStrongWindChecked(final boolean checked) {
        if (checked) {
            mState.wind = WindType.STRONG;
            onStateChanged();
        }
    }

    private void onStateChanged() {
        changeSelectedCloudiness(mState.cloudiness);
        final boolean sendButtonEnabled =
                mState.cloudiness != Cloudiness.NONE &&
                !Double.isNaN(mState.temperature) &&
                mState.wind != WindType.NONE;
        mView.setSendButtonEnabled(sendButtonEnabled);
    }

    public void onSendButtonClicked() {
        mView.putUsersData(new UserReport(mState.cloudiness, mState.temperature, mState.wind));
    }

    private class State {

        private Cloudiness cloudiness = Cloudiness.NONE;
        private double temperature = Double.NaN;
        private WindType wind = WindType.NONE;
    }

    public interface View {

        void selectClearCloudiness();

        void selectCloudyCloudiness();

        void selectOvercastCloudiness();

        void selectSleetCloudiness();

        void selectRainCloudiness();

        void selectSnowCloudiness();

        void unselectCloudiness();

        void setSendButtonEnabled(boolean sendButtonEnabled);

        void putUsersData(UserReport userReport);

    }
}
