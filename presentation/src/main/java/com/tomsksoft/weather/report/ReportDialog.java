package com.tomsksoft.weather.report;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.tomsksoft.domain.model.UserReport;
import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseDialog;
import com.tomsksoft.weather.view.AfterTextChangedListener;

import java.util.Objects;

public class ReportDialog extends BaseDialog {

    private ReportPresenter mPresenter;
    private View mClearCloudinessButton;
    private View mCloudyCloudinessButton;
    private View mOvercastCloudinessButton;
    private View mSleetCloudinessButton;
    private View mRainCloudinessButton;
    private View mSnowCloudinessButton;

    private Button mButtonSend;
    private ImageButton mButtonClose;

    public static final String TAG_REPORT_CLOUDINESS = "Cloudiness";
    public static final String TAG_REPORT_TEMPERATURE = "Temperature";
    public static final String TAG_REPORT_WIND = "Wind";

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_report, null);
        builder.setView(view).setPositiveButton(
                getString(R.string.send),
                (dialogInterface, which) -> mPresenter.onSendButtonClicked()
        );

        mClearCloudinessButton = view.findViewById(R.id.card_clear);
        mClearCloudinessButton.setOnClickListener(v -> mPresenter.onClearCloudinessClicked());

        mCloudyCloudinessButton = view.findViewById(R.id.card_cloudy);
        mCloudyCloudinessButton.setOnClickListener(v -> mPresenter.onCloudyCloudinessClicked());

        mOvercastCloudinessButton = view.findViewById(R.id.card_overcast);
        mOvercastCloudinessButton.setOnClickListener(v -> mPresenter.onOvercastCloudinessClicked());

        mSleetCloudinessButton = view.findViewById(R.id.card_sleet);
        mSleetCloudinessButton.setOnClickListener(v -> mPresenter.onSleetCloudinessClicked());

        mRainCloudinessButton = view.findViewById(R.id.card_rain);
        mRainCloudinessButton.setOnClickListener(v -> mPresenter.onRainCloudinessClicked());

        mSnowCloudinessButton = view.findViewById(R.id.card_snow);
        mSnowCloudinessButton.setOnClickListener(v -> mPresenter.onSnowCloudinessClicked());

        final EditText temperatureEditText = view.findViewById(R.id.temperature_report_edittext);
        temperatureEditText.addTextChangedListener((AfterTextChangedListener) text -> mPresenter.onTemperatureChanged(text));

        final RadioButton calmWindButton = view.findViewById(R.id.radio_button_wind_calm);
        calmWindButton.setOnCheckedChangeListener((v, checked) -> mPresenter.onCalmWindChecked(checked));

        final RadioButton lightWindButton = view.findViewById(R.id.radio_button_wind_light);
        lightWindButton.setOnCheckedChangeListener((v, checked) -> mPresenter.onLightWindChecked(checked));

        final RadioButton moderateWindButton = view.findViewById(R.id.radio_button_wind_moderate);
        moderateWindButton.setOnCheckedChangeListener((v, checked) -> mPresenter.onModerateWindChecked(checked));

        final RadioButton strongWindButton = view.findViewById(R.id.radio_button_wind_strong);
        strongWindButton.setOnCheckedChangeListener((v, checked) -> mPresenter.onStrongWindChecked(checked));

        mButtonClose = view.findViewById(R.id.negative_button);

        final AlertDialog dialog = builder.show();

        mButtonSend = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        mButtonSend.setEnabled(false);
        mButtonClose.setOnClickListener(v -> dismiss());
        return dialog;
    }


    @Override
    protected void onCreatePresenter() {
        super.onCreatePresenter();
        mPresenter = new ReportPresenter(mView);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.stop();
    }

    private final ReportPresenter.View mView = new ReportPresenter.View() {

        @Override
        public void selectClearCloudiness() {
            mClearCloudinessButton.setSelected(true);
        }

        @Override
        public void selectCloudyCloudiness() {
            mCloudyCloudinessButton.setSelected(true);
        }

        @Override
        public void selectOvercastCloudiness() {
            mOvercastCloudinessButton.setSelected(true);
        }

        @Override
        public void selectSleetCloudiness() {
            mSleetCloudinessButton.setSelected(true);
        }

        @Override
        public void selectRainCloudiness() {
            mRainCloudinessButton.setSelected(true);
        }

        @Override
        public void selectSnowCloudiness() {
            mSnowCloudinessButton.setSelected(true);
        }

        @Override
        public void unselectCloudiness() {
            mClearCloudinessButton.setSelected(false);
            mCloudyCloudinessButton.setSelected(false);
            mOvercastCloudinessButton.setSelected(false);
            mSleetCloudinessButton.setSelected(false);
            mRainCloudinessButton.setSelected(false);
            mSnowCloudinessButton.setSelected(false);
        }

        @Override
        public void setSendButtonEnabled(final boolean sendButtonEnabled) {
            if (sendButtonEnabled) {
                mButtonSend.setEnabled(true);
            }
        }

        @Override
        public void putUsersData(final UserReport userReport) {
            final Intent intent = new Intent();
            intent.putExtra(TAG_REPORT_CLOUDINESS, userReport.getCloudiness().toString())
                  .putExtra(TAG_REPORT_TEMPERATURE, userReport.getTemperature())
                  .putExtra(TAG_REPORT_WIND, userReport.getWind().toString());

            Objects.requireNonNull(getTargetFragment()).onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        }

    };
}
