package com.tomsksoft.weather.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.tomsksoft.weather.R;
import com.tomsksoft.weather.base.BaseActivity;
import com.tomsksoft.weather.home.HomeFragment;

public class MainActivity extends BaseActivity {

    private MainPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment homeFragment = new HomeFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container_main, homeFragment).commit();
    }

    @Override
    protected void onCreatePresenter() {
        super.onCreatePresenter();
        mPresenter = new MainPresenter();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.stop();
    }
}
